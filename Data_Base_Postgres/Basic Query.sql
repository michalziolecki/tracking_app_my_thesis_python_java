select * from keylogger_wiadomosci;
delete from keylogger_wiadomosci;

select * from uzytkownicy_keylogger;
insert into uzytkownicy_keylogger (nazwa_komputera, Nazwa_Uzytkownika_Na_Komputerze, opis) values('komputer-test', 'Admin-test', 'test');
delete from uzytkownicy_keylogger;

select * from keylogger_filtr;
insert into keylogger_filtr (id_uzytkownicy_keylogger, filtr) values ('1', 'kwejk');
delete from keylogger_filtr;

select * from ustawienia_keylogger;
update ustawienia_keylogger set czy_zbierac_wiadomosc='1' where id_uzytkownicy_keylogger='1';

select * from informacje_dyski_zdalne;
insert into informacje_dyski_zdalne 
(id_uzytkownicy_keylogger, data_operacji, rozpoznany_jako, status_operacji, nazwa_dysku, opis, system_plikow, wolna_przestrzen, calkowita_przestrzen, nazwa_woluminu, identyfikator_woluminu)
values('1', '2001-09-28 01:00', 'pendrive', 'IN', 'F:','opis', 'system_plikow', 123456789, 12345678, 'nazwa_woluminu', 'identyfikator_wol');
delete from informacje_dyski_zdalne;

select * from niesledzone_partycje;
delete from niesledzone_partycje where id=45;
	
insert into skanowanie_dyskow_zdalnych (ID_Uzytkownicy_Keylogger, id_dysku, nazwa_dysku, data_operacji, nazwa_pliku,
										sciezka_pliku, typ_pliku, jest_binarny, rozszerzenie, rozmiar)
						values (1,6,'F:', '2001-09-28 01:00', 'nazwa_pliku', 'sciezka_pliku', 'typ_pliku', 1, 'txt', 123456789);
select * from skanowanie_dyskow_zdalnych;

delete from skanowanie_dyskow_zdalnych;

select * from sledzenie_napedu_plyt;
update sledzenie_napedu_plyt set czy_sledzic='1', czy_skanowac='1' where id_uzytkownicy_keylogger='1';
delete from sledzenie_napedu_plyt;

select * from sledzenie_usb;
update sledzenie_usb set czy_sledzic='1', czy_skanowac='1' where id_uzytkownicy_keylogger='1';
delete from sledzenie_usb;

select * from urzadzenia_sledzenie order by data_operacji desc;
insert into urzadzenia_sledzenie 
(id_uzytkownicy_keylogger, data_operacji, rozpoznany_jako, status_operacji, nazwa_urzadzenia, opis, producent, id_urzadzenia, guid_urzadzenia, typ_urzadzenia)
values('1', '2001-09-28 01:00', 'pendrive', 'IN', 'PEN_TEST','opis', 'inventor', 'id_device', 'ADSF2354SADFG', 'type');
delete from urzadzenia_sledzenie;

select * from blokowanie_usb;
update blokowanie_usb set blokuj_porty='0' where id_uzytkownicy_keylogger='1';
delete from blokowanie_usb where id=16;



