CREATE TABLE Uzytkownicy_Keylogger 
(
    ID INT Primary Key GENERATED ALWAYS AS IDENTITY,
    Nazwa_Komputera VARCHAR(50) NOT NULL,
	Nazwa_Uzytkownika_Na_Komputerze varchar(50) not null,
	Opis varchar(250)
);

create table Ustawienia_Keylogger 
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Czy_Zbierac_Wiadomosc smallint not null
);

create table Keylogger_Filtr 
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Filtr varchar(50) not null
);

create table Keylogger_Wiadomosci 
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Wiadomosc varchar(2000) not null,
	Kiedy_Zebrano_Wiadomosc timestamp not null
);

create table urzadzenia_sledzenie
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	data_operacji timestamp not null,
	rozpoznany_jako varchar(50) not null,
	status_operacji varchar(3) not null,
	nazwa_urzadzenia varchar(250) not null,
	opis varchar(250) not null,
	producent varchar(250) not null,
	id_urzadzenia varchar(250) not null,
	guid_urzadzenia varchar(250) not null,
	typ_urzadzenia varchar(100) not null
);

create table blokowanie_usb
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	blokuj_porty smallint not null
);

create table uprawnienia_administracyjne
(
	ID int primary key generated always as identity,
	login_administratora varchar(250) not null,
	haslo_administratora varchar(250) not null
);

create table informacje_dyski_zdalne
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	data_operacji timestamp not null,
	rozpoznany_jako varchar(50) not null,
	status_operacji varchar(3) not null,
	nazwa_dysku varchar(3) not null,
	opis varchar(100) not null,
	system_plikow varchar(10) not null,
	wolna_przestrzen bigint not null,
	calkowita_przestrzen bigint not null,
	nazwa_woluminu varchar(100) not null,
	identyfikator_woluminu varchar(100) not null
);

create table sledzenie_usb
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	czy_sledzic smallint not null,
	czy_skanowac smallint not null
);

create table sledzenie_napedu_plyt
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	czy_sledzic smallint not null,
	czy_skanowac smallint not null
);

create table skanowanie_dyskow_zdalnych			
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	id_dysku int references informacje_dyski_zdalne(ID) not null,
	nazwa_dysku varchar(3) not null,
	data_operacji timestamp not null,
	nazwa_pliku varchar(250) not null,
	sciezka_pliku varchar(512) not null,
	typ_pliku varchar(100) not null,
	jest_binarny smallint not null,
	rozszerzenie varchar(10) not null,
	rozmiar bigint not null
);

create table niesledzone_partycje
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	nazwa_partycji_dysku varchar(3) not null
);

