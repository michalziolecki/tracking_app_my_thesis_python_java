--Jeśli posiadasz Triggery i TRIGGER Function to usuń je przed ponownym wykonaniem

CREATE OR REPLACE FUNCTION add_settings()
  RETURNS trigger AS
$BODY$
BEGIN

insert into Ustawienia_Keylogger (ID_Uzytkownicy_Keylogger,Czy_Zbierac_Wiadomosc)
values (new.ID,1);

insert into blokowanie_usb (ID_Uzytkownicy_Keylogger,blokuj_porty)
values (new.ID,0);

insert into sledzenie_usb (ID_Uzytkownicy_Keylogger,czy_sledzic, czy_skanowac)
values (new.ID,1,1);

insert into sledzenie_napedu_plyt (ID_Uzytkownicy_Keylogger,czy_sledzic, czy_skanowac)
values (new.ID,1,1);

insert into niesledzone_partycje (ID_Uzytkownicy_Keylogger, nazwa_partycji_dysku)
values (new.ID,'C:');

insert into niesledzone_partycje (ID_Uzytkownicy_Keylogger, nazwa_partycji_dysku)
values (new.ID,'D:');
 
 RETURN NEW;
END;
$BODY$

LANGUAGE plpgsql VOLATILE
COST 100;


CREATE TRIGGER IF_Add_User
  after insert
  ON Uzytkownicy_Keylogger
  FOR EACH ROW
  EXECUTE PROCEDURE add_settings();
  
 ---------------------------------------------------------------------------------- 



  
  
  
 CREATE OR REPLACE FUNCTION delete_all()
  RETURNS trigger AS
$BODY$
BEGIN
 delete from Ustawienia_Keylogger where ID_Uzytkownicy_Keylogger = old.id;
 delete from Keylogger_Filtr where ID_Uzytkownicy_Keylogger = old.id;
 delete from Keylogger_Wiadomosci where ID_Uzytkownicy_Keylogger = old.id;
 delete from urzadzenia_sledzenie where ID_Uzytkownicy_Keylogger = old.id;
 delete from blokowanie_usb where ID_Uzytkownicy_Keylogger = old.id;
 delete from sledzenie_usb where ID_Uzytkownicy_Keylogger = old.id;
 delete from sledzenie_napedu_plyt where ID_Uzytkownicy_Keylogger = old.id;
 delete from skanowanie_dyskow_zdalnych where ID_Uzytkownicy_Keylogger = old.id;
 delete from informacje_dyski_zdalne where ID_Uzytkownicy_Keylogger = old.id;
 delete from niesledzone_partycje where ID_Uzytkownicy_Keylogger = old.id;

 
 RETURN OLD;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE TRIGGER IF_delete_User
  before delete
  ON Uzytkownicy_Keylogger
  FOR EACH ROW
  EXECUTE PROCEDURE delete_all();
  
  
  --------------------------------------------------------------------------------------
  


create FUNCTION blocked_double_user()
  RETURNS trigger AS
$BODY$
BEGIN
   IF (SELECT id FROM Uzytkownicy_Keylogger WHERE nazwa_komputera = NEW.nazwa_komputera and Nazwa_Uzytkownika_Na_Komputerze = NEW.Nazwa_Uzytkownika_Na_Komputerze) is not null THEN
      RAISE EXCEPTION 'Uzytkownik juz istnieje w bazie danych';
   END IF;
   RETURN NEW;
 
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
  
create TRIGGER blocked_double_users
BEFORE INSERT ON uzytkownicy_keylogger
FOR EACH ROW EXECUTE PROCEDURE blocked_double_user();