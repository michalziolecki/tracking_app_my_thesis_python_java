package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.TrackDiscDriverSettingsDAO;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;
import org.umk.thesis.ziolecki.logit.entity.TrackDiscDriverSettings;
import org.umk.thesis.ziolecki.logit.entity.TrackUSBSettings;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceTrackDiscDriverSettingsDAO {
    private TrackDiscDriverSettingsDAO trackDiscDriverSettingsDAO;
    private UserKeyloggerDAO userKeyloggerDAO;

    @Autowired
    public ServiceTrackDiscDriverSettingsDAO(TrackDiscDriverSettingsDAO trackDiscDriverSettingsDAO,
                                      UserKeyloggerDAO userKeyloggerDAO){
        this.trackDiscDriverSettingsDAO = trackDiscDriverSettingsDAO;
        this.userKeyloggerDAO = userKeyloggerDAO;
    }


    public TrackDiscDriverSettings getDiscDriverSettingsById(int id){
        return this.trackDiscDriverSettingsDAO.findById(id).get();
    }

    public TrackDiscDriverSettings getDiscDriverSettingsByUserId(int id){
        UserKeylogger userKeylogger = this.userKeyloggerDAO.findById(id).get();
        return this.trackDiscDriverSettingsDAO.findByUser(userKeylogger);
    }

    public TrackDiscDriverSettings getDiscDriverSettingsByUser(UserKeylogger userKeylogger){
        return this.trackDiscDriverSettingsDAO.findByUser(userKeylogger);
    }

    public List<TrackDiscDriverSettings> getAllDiscDriverSettings(){
        List<TrackDiscDriverSettings> allTrackUSBSettings = new ArrayList<>();
        this.trackDiscDriverSettingsDAO.findAll().forEach(allTrackUSBSettings::add);
        return allTrackUSBSettings;
    }

    public TrackDiscDriverSettings updateDiscDriverSettings(TrackDiscDriverSettings trackDiscDriverSettings){
        return this.trackDiscDriverSettingsDAO.save(trackDiscDriverSettings);
    }
}
