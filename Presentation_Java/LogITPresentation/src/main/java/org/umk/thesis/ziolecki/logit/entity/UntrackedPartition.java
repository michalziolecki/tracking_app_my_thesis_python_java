package org.umk.thesis.ziolecki.logit.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "niesledzone_partycje")
public class UntrackedPartition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(nullable = false, name = "id_uzytkownicy_keylogger")
    private UserKeylogger user;
    @Column(nullable = false, name = "nazwa_partycji_dysku")
    private String diskName;
}
