package org.umk.thesis.ziolecki.logit.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.umk.thesis.ziolecki.logit.entity.CachedDevice;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.List;

@Repository
public interface CachedDeviceDAO extends CrudRepository<CachedDevice, Integer> {
    List<CachedDevice> findByUser(UserKeylogger userKeylogger);
}
