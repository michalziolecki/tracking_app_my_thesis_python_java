package org.umk.thesis.ziolecki.logit.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.umk.thesis.ziolecki.logit.entity.*;
import org.umk.thesis.ziolecki.logit.service.*;

import java.util.List;

@Controller
public class RestControllerPeriphery {

    private ServiceTrackUSBSettingsDAO serviceTrackUSBSettingsDAO;
    private ServiceUserKeyloggerDAO serviceUserKeyloggerDAO;
    private ServiceTrackDiscDriverSettingsDAO serviceTrackDiscDriverSettingsDAO;
    private ServiceBlockUSBDAO serviceBlockUSBDAO;
    private ServiceCachedDeviceDAO serviceCachedDeviceDAO;
    private ServiceRemoteDiskInfoDAO serviceRemoteDiskInfoDAO;
    private ServiceScannedDiskInfoDAO serviceScannedDiskInfoDAO;
    private ServiceUntrackedPartitionDAO serviceUntrackedPartitionDAO;

    // Track USB settings paths
    @Autowired
    public RestControllerPeriphery(ServiceTrackUSBSettingsDAO serviceTrackUSBSettingsDAO,
                                   ServiceUserKeyloggerDAO serviceUserKeyloggerDAO,
                                   ServiceTrackDiscDriverSettingsDAO serviceTrackDiscDriverSettingsDAO,
                                   ServiceBlockUSBDAO serviceBlockUSBDAO,
                                   ServiceCachedDeviceDAO serviceCachedDeviceDAO,
                                   ServiceRemoteDiskInfoDAO serviceRemoteDiskInfoDAO,
                                   ServiceScannedDiskInfoDAO serviceScannedDiskInfoDAO,
                                   ServiceUntrackedPartitionDAO serviceUntrackedPartitionDAO){
        this.serviceTrackUSBSettingsDAO = serviceTrackUSBSettingsDAO;
        this.serviceUserKeyloggerDAO =  serviceUserKeyloggerDAO;
        this.serviceTrackDiscDriverSettingsDAO = serviceTrackDiscDriverSettingsDAO;
        this.serviceBlockUSBDAO = serviceBlockUSBDAO;
        this.serviceCachedDeviceDAO = serviceCachedDeviceDAO;
        this.serviceRemoteDiskInfoDAO = serviceRemoteDiskInfoDAO;
        this.serviceScannedDiskInfoDAO = serviceScannedDiskInfoDAO;
        this.serviceUntrackedPartitionDAO = serviceUntrackedPartitionDAO;
    }

    @RequestMapping(value = {"/track-usb-set"}, method = RequestMethod.GET)
    public String getUSBSettings(Model model, @RequestParam(required = false) Integer id) {
        if (id != null) {
            TrackUSBSettings trackUSBSettings = this.serviceTrackUSBSettingsDAO.getUSBSettingsByUserId(id);
            model.addAttribute("trackUSBSettings", trackUSBSettings);
            return "trackUSBSettings";
        } else {
            List<TrackUSBSettings> trackUSBSettings = this.serviceTrackUSBSettingsDAO.getAllUSBSettings();
            model.addAttribute("trackUSBSettings", trackUSBSettings);
            return "trackUSBSettingsAll";
        }
    }

    @RequestMapping(value = {"/update-usb-track-settings"}, method = RequestMethod.GET)
    public String updateUSBSettingsForm(Model model, @RequestParam Integer id) {
        TrackUSBSettings trackUSBSettings = this.serviceTrackUSBSettingsDAO.getUSBSettingsById(id);
        model.addAttribute("trackUSBSettings", trackUSBSettings);
        return "updateTrackUSBSettings";
    }

    @RequestMapping(value = {"/updatedUSBTrackSettings"}, method = RequestMethod.POST)
    public String updateUSBSettingsAction(Model model, @ModelAttribute TrackUSBSettings trackUSBSettings) {
        TrackUSBSettings updatedTrackUSBSettings = this.serviceTrackUSBSettingsDAO.updateUSBSettings(trackUSBSettings);
        model.addAttribute("trackUSBSettings", this.serviceTrackUSBSettingsDAO
                .getUSBSettingsByUserId(trackUSBSettings.getUser().getId()));
        return "trackUSBSettings";
    }

    // Track Disc Drivers settings paths
    @RequestMapping(value = {"/track-disk-drivers-set"}, method = RequestMethod.GET)
    public String getDiscDriversSettings(Model model, @RequestParam(required = false) Integer id) {
        if (id != null) {
            TrackDiscDriverSettings trackDiscDriverSettings = this.serviceTrackDiscDriverSettingsDAO.
                    getDiscDriverSettingsByUserId(id);
            model.addAttribute("trackDiskDriversSettings", trackDiscDriverSettings);
            return "trackDiskDriversSettings";
        } else {
            List<TrackDiscDriverSettings> trackDiscDriverSettings = this.serviceTrackDiscDriverSettingsDAO
                    .getAllDiscDriverSettings();
            model.addAttribute("trackDiskDriversSettings", trackDiscDriverSettings);
            return "trackDiskDriversSettingsAll";
        }
    }

    @RequestMapping(value = {"/update-disk-drivers-track-settings"}, method = RequestMethod.GET)
    public String updateDiscDriversSettingsForm(Model model, @RequestParam Integer id) {
        TrackDiscDriverSettings trackDiscDriverSettings = this.serviceTrackDiscDriverSettingsDAO.getDiscDriverSettingsById(id);
        model.addAttribute("trackDiskDriversSettings", trackDiscDriverSettings);
        return "updateTrackDiskDriversSettings";
    }

    @RequestMapping(value = {"/updatedDiskDriversTrackSettings"}, method = RequestMethod.POST)
    public String updateDiscDriversSettingsAction(Model model, @ModelAttribute TrackDiscDriverSettings trackDiscDriverSettings) {
        TrackDiscDriverSettings updatedTrackDiscDriverSettings = this.serviceTrackDiscDriverSettingsDAO.updateDiscDriverSettings(trackDiscDriverSettings);
        model.addAttribute("trackDiskDriversSettings", this.serviceTrackDiscDriverSettingsDAO
                .getDiscDriverSettingsByUserId(updatedTrackDiscDriverSettings.getUser().getId()));
        return "trackDiskDriversSettings";
    }

    //block USB
    @RequestMapping(value = {"/block-usb"}, method = RequestMethod.GET)
    public String getBlockUSBSettings(Model model, @RequestParam(required = false) Integer id) {
        if (id != null) {
            BlockUSBSettings blockUSBSettings = this.serviceBlockUSBDAO.getBlockUSBByUserId(id);
            model.addAttribute("blockUSBSettings", blockUSBSettings);
            return "blockUSBSettings";
        } else {
            List<BlockUSBSettings> blockUSBSettings = this.serviceBlockUSBDAO
                    .getAllBlockUSBStatuses();
            model.addAttribute("allBlockUSBBlockUSBSettings", blockUSBSettings);
            return "allBlockUSBSettings";
        }
    }

    @RequestMapping(value = {"/update-block-usb-settings"}, method = RequestMethod.GET)
    public String updateBlockUSBSettingsForm(Model model, @RequestParam Integer id) {
        BlockUSBSettings blockUSBSettings = this.serviceBlockUSBDAO.getBlockUSBById(id);
        model.addAttribute("blockUSBSettings", blockUSBSettings);
        return "updateBlockUSBSettings";
    }

    @RequestMapping(value = {"/updatedBlockUSBSettings"}, method = RequestMethod.POST)
    public String updateBlockUSBSettings(Model model, @ModelAttribute BlockUSBSettings blockUSBSettings) {
        BlockUSBSettings updatedBlockUSBSettings = this.serviceBlockUSBDAO.updateBlockUSB(blockUSBSettings);
        model.addAttribute("blockUSBSettings", this.serviceBlockUSBDAO
                .getBlockUSBByUserId(updatedBlockUSBSettings.getUser().getId()));
        return "blockUSBSettings";
    }

    @RequestMapping(value = {"/tracked-devices"}, method = RequestMethod.GET)
    public String getCachedDevices(Model model, @RequestParam(required = false) Integer id) {
        if (id != null) {
            List<CachedDevice> cachedDevices = this.serviceCachedDeviceDAO.getCachedDevicesByUserId(id);
            model.addAttribute("cachedDevices", cachedDevices);
            UserKeylogger userKeylogger =  this.serviceUserKeyloggerDAO.getUserById(id);
            model.addAttribute("user", userKeylogger);
            return "cachedDevices";
        } else {
            List<CachedDevice> cachedDevices = this.serviceCachedDeviceDAO.getAllCachedDevices();
            model.addAttribute("cachedDevices", cachedDevices);
            return "allCachedDevices";
        }
    }

    @RequestMapping(value = {"/cached-disk-info"}, method = RequestMethod.GET)
    public String getRemoteDiskInfo(Model model, @RequestParam(required = false) Integer id) {
        if (id != null) {
            List<RemoteDiskInfo> remoteDiskInfoList = this.serviceRemoteDiskInfoDAO.getCachedDevicesByUserId(id);
            model.addAttribute("remoteDiskInfoList", remoteDiskInfoList);
            UserKeylogger userKeylogger =  this.serviceUserKeyloggerDAO.getUserById(id);
            model.addAttribute("user", userKeylogger);
            return "remoteDiskInfoList";
        } else {
            List<RemoteDiskInfo> remoteDiskInfoList = this.serviceRemoteDiskInfoDAO.getAllRemoteDiskInfo();
            model.addAttribute("remoteDiskInfoList", remoteDiskInfoList);
            return "allRemoteDiskInfoList";
        }
    }

    @RequestMapping(value = {"/scanned-disks-info"}, method = RequestMethod.GET)
    public String getScannedDiskInfo(Model model, @RequestParam(required = false) Integer id) {
        if (id != null) {
            List<ScannedDiskInfo> scannedDiskInfoList = this.serviceScannedDiskInfoDAO.getScannedDiskInfoByUserId(id);
            model.addAttribute("scannedDiskInfoList", scannedDiskInfoList);
            UserKeylogger userKeylogger =  this.serviceUserKeyloggerDAO.getUserById(id);
            model.addAttribute("user", userKeylogger);
            return "scannedDiskInfoList";
        } else {
            List<ScannedDiskInfo> scannedDiskInfoList = this.serviceScannedDiskInfoDAO.getAllScannedDiskInfo();
            model.addAttribute("scannedDiskInfoList", scannedDiskInfoList);
            return "allScannedDiskInfoList";
        }
    }

    // untracked partitions
    @RequestMapping(value = {"/untracked-partitions"}, method = RequestMethod.GET)
    public String getUntrackedPartitions(Model model, @RequestParam(required = false) Integer id) {
        if (id != null) {
            List<UntrackedPartition> untrackedPartitions = this.serviceUntrackedPartitionDAO
                    .getUntrackedPartitionsByUserId(id);
            UserKeylogger userKeylogger = this.serviceUserKeyloggerDAO.getUserById(id);
            model.addAttribute("untrackedPartitions", untrackedPartitions);
            model.addAttribute("user", userKeylogger);
            return "untrackedPartitions";
        } else {
            List<UntrackedPartition> untrackedPartitions = this.serviceUntrackedPartitionDAO.getAllUntrackedPartitions();
            model.addAttribute("allUntrackedPartitions", untrackedPartitions);
            return "allUntrackedPartitions";
        }
    }

    @RequestMapping(value = {"/add-untracked-partition"}, method = RequestMethod.GET)
    public String addUntrackedPartitionForm(Model model, @RequestParam Integer id) {
        UserKeylogger userKeylogger = this.serviceUserKeyloggerDAO.getUserById(id);
        model.addAttribute("user", userKeylogger);
        return "addUntrackedPartition";
    }

    @RequestMapping(value = {"/addUntrackedPartition"}, method = RequestMethod.POST)
    public String addUntrackedPartition(Model model, @ModelAttribute UntrackedPartition untrackedPartition) {
        UntrackedPartition addedUntrackedPartition = this.serviceUntrackedPartitionDAO
                .addUntrackedPartition(untrackedPartition);
        List<UntrackedPartition> untrackedPartitions = this.serviceUntrackedPartitionDAO
                .getUntrackedPartitionsByUserId(addedUntrackedPartition.getUser().getId());
        model.addAttribute("untrackedPartitions", untrackedPartitions);
        model.addAttribute("user", addedUntrackedPartition.getUser());
        return "untrackedPartitions";

    }

    @RequestMapping(value = {"/update-untracked-partition"}, method = RequestMethod.GET)
    public String updateUntrackedPartitionForm(Model model, @RequestParam Integer id) {
        UntrackedPartition untrackedPartition = this.serviceUntrackedPartitionDAO.getUntrackedPartitionById(id);
        model.addAttribute("untrackedPartition", untrackedPartition);
        return "updateUntrackedPartition";
    }

    @RequestMapping(value = {"/updateUntrackedPartition"}, method = RequestMethod.POST)
    public String updateUntrackedPartition(Model model, @ModelAttribute UntrackedPartition untrackedPartition) {
        UntrackedPartition updatedUntrackedPartition = this.serviceUntrackedPartitionDAO
                .updateUntrackedPartition(untrackedPartition);
        List<UntrackedPartition> untrackedPartitions = this.serviceUntrackedPartitionDAO
                .getUntrackedPartitionsByUserId(updatedUntrackedPartition.getUser().getId());
        model.addAttribute("untrackedPartitions", untrackedPartitions);
        model.addAttribute("user", updatedUntrackedPartition.getUser());
        return "untrackedPartitions";
    }

    @RequestMapping(value = {"/deleteUntrackedPartition"}, method = RequestMethod.GET)
    public String deleteUntrackedPartition(Model model, @RequestParam Integer id) {
        UntrackedPartition untrackedPartition = this.serviceUntrackedPartitionDAO.getUntrackedPartitionById(id);
        UserKeylogger user = untrackedPartition.getUser();
        model.addAttribute("user", user);
        this.serviceUntrackedPartitionDAO.daleteUntrackedPartition(untrackedPartition);
        List<UntrackedPartition> untrackedPartitions = this.serviceUntrackedPartitionDAO
                .getUntrackedPartitionsByUserId(user.getId());
        model.addAttribute("untrackedPartitions", untrackedPartitions);
        return "untrackedPartitions";

    }
}
