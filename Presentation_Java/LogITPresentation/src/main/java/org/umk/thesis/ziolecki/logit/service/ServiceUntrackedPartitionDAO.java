package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.UntrackedPartitionDAO;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;
import org.umk.thesis.ziolecki.logit.entity.UntrackedPartition;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceUntrackedPartitionDAO {
    private UntrackedPartitionDAO untrackedPartitionDAO;
    private UserKeyloggerDAO userKeyloggerDAO;

    @Autowired
    public  ServiceUntrackedPartitionDAO(UntrackedPartitionDAO untrackedPartitionDAO, UserKeyloggerDAO userKeyloggerDAO){
        this.untrackedPartitionDAO = untrackedPartitionDAO;
        this.userKeyloggerDAO = userKeyloggerDAO;
    }

    public List<UntrackedPartition> getAllUntrackedPartitions(){
        List<UntrackedPartition> untrackedPartitions = new ArrayList<>();
        this.untrackedPartitionDAO.findAll().forEach(untrackedPartitions::add);
        return untrackedPartitions;
    }

    public List<UntrackedPartition> getUntrackedPartitionsByUserId(int id){
        UserKeylogger userKeylogger = this.userKeyloggerDAO.findById(id).get();
        return this.untrackedPartitionDAO.findByUser(userKeylogger);
    }

    public UntrackedPartition addUntrackedPartition(UntrackedPartition keyloggerFilter){
        return this.untrackedPartitionDAO.save(keyloggerFilter);
    }

    public UntrackedPartition updateUntrackedPartition(UntrackedPartition untrackedPartition){
        return this.untrackedPartitionDAO.save(untrackedPartition);
    }

    public UntrackedPartition getUntrackedPartitionById(Integer id){
        return this.untrackedPartitionDAO.findById(id).get();
    }

    public void daleteUntrackedPartition(UntrackedPartition untrackedPartition){
        this.untrackedPartitionDAO.delete(untrackedPartition);
    }
}
