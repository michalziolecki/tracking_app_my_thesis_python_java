package org.umk.thesis.ziolecki.logit.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RestControllerMainWeb {

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String getMainWebsite() {
        return "index";
    }
}
