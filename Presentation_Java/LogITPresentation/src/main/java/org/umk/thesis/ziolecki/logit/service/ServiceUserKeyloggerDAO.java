package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceUserKeyloggerDAO {

    private UserKeyloggerDAO userKeyloggerDAO;

    @Autowired
    public ServiceUserKeyloggerDAO(UserKeyloggerDAO userKeyloggerDAO){
        this.userKeyloggerDAO = userKeyloggerDAO;
    }

    public List<UserKeylogger> getAllUsers(){
        List<UserKeylogger> users = new ArrayList<>();
        this.userKeyloggerDAO.findAll().forEach(user -> users.add(user));
        return  users;
    }

    public UserKeylogger getUserById(Integer id){
        return this.userKeyloggerDAO.findById(id).get();
    }

    public UserKeylogger getUserByName(String name){
        return this.userKeyloggerDAO.findByUserName(name);
    }

}
