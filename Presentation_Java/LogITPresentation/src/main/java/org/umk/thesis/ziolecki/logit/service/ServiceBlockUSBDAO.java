package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.BlockUSBSettingsDAO;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;
import org.umk.thesis.ziolecki.logit.entity.BlockUSBSettings;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceBlockUSBDAO {

    private BlockUSBSettingsDAO blockUSBSettingsDAO;
    private UserKeyloggerDAO userKeyloggerDAO;

    @Autowired
    public ServiceBlockUSBDAO(BlockUSBSettingsDAO blockUSBSettingsDAO,
                              UserKeyloggerDAO userKeyloggerDAO){
        this.blockUSBSettingsDAO = blockUSBSettingsDAO;
        this.userKeyloggerDAO = userKeyloggerDAO;
    }

    public List<BlockUSBSettings> getAllBlockUSBStatuses(){
        List<BlockUSBSettings> blockUSBSettings = new ArrayList<>();
        this.blockUSBSettingsDAO.findAll().forEach(blockUSBSettings::add);
        return blockUSBSettings;
    }

    public BlockUSBSettings getBlockUSBById(int id){
        return this.blockUSBSettingsDAO.findById(id).get();
    }

    public BlockUSBSettings getBlockUSBByUserId(int id){
        UserKeylogger userKeylogger = this.userKeyloggerDAO.findById(id).get();
        return this.blockUSBSettingsDAO.findByUser(userKeylogger);
    }

    public BlockUSBSettings updateBlockUSB(BlockUSBSettings blockUSBSettings){
        return this.blockUSBSettingsDAO.save(blockUSBSettings);
    }

}
