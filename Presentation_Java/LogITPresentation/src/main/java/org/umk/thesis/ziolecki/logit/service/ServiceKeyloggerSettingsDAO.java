package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.KeyloggerSettingsDAO;
import org.umk.thesis.ziolecki.logit.entity.KeyloggerSettings;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceKeyloggerSettingsDAO {

    private KeyloggerSettingsDAO keyloggerSettingsDAO;

    @Autowired
    public ServiceKeyloggerSettingsDAO(KeyloggerSettingsDAO keyloggerSettingsDAO){
        this.keyloggerSettingsDAO = keyloggerSettingsDAO;
    }

    public List<KeyloggerSettings> getAllKeyloggerSettings(){
        List<KeyloggerSettings> keyloggerSettings = new ArrayList<>();
        this.keyloggerSettingsDAO.findAll().forEach(keyloggerSet -> keyloggerSettings.add(keyloggerSet));
        return keyloggerSettings;
    }

    public KeyloggerSettings getKeylogSettingsById(int id){
        return this.keyloggerSettingsDAO.findById(id).get();
    }

    public KeyloggerSettings updateKeyloggerSettings(KeyloggerSettings keyloggerSettings){
        return this.keyloggerSettingsDAO.save(keyloggerSettings);
    }


}
