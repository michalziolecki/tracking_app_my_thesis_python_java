package org.umk.thesis.ziolecki.logit.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ustawienia_keylogger")
public class KeyloggerSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne(targetEntity = UserKeylogger.class, cascade = CascadeType.ALL)
    @JoinColumn(nullable = false, name = "id_uzytkownicy_keylogger", referencedColumnName = "id")
    private UserKeylogger user;
    @Column(nullable = false, name = "czy_zbierac_wiadomosc", columnDefinition = "SMALLINT")
    @Type(type = "org.hibernate.type.ShortType")
    private Short trackMsg;
}
