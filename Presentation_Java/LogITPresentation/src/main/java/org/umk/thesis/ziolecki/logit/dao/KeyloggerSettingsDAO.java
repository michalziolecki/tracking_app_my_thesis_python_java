package org.umk.thesis.ziolecki.logit.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.umk.thesis.ziolecki.logit.entity.KeyloggerSettings;

@Repository
public interface KeyloggerSettingsDAO extends CrudRepository<KeyloggerSettings, Integer> {
}
