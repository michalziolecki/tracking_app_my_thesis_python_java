package org.umk.thesis.ziolecki.logit.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

@Repository
public interface UserKeyloggerDAO extends CrudRepository<UserKeylogger, Integer> {
    UserKeylogger findByUserName(String name);
}
