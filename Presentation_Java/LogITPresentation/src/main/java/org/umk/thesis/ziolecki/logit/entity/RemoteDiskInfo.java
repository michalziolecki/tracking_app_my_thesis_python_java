package org.umk.thesis.ziolecki.logit.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.type.BigDecimalType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "informacje_dyski_zdalne")
public class RemoteDiskInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(nullable = false, name = "id_uzytkownicy_keylogger")
    private UserKeylogger user;
    @Column(nullable = false, name = "data_operacji")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;
    @Column(nullable = false, name = "rozpoznany_jako")
    String recognizeInfo;
    @Column(nullable = false, name = "status_operacji")
    String operationStatus;
    @Column(nullable = false, name = "nazwa_dysku")
    String deviceName;
    @Column(nullable = false, name = "opis")
    String description;
    @Column(nullable = false, name = "system_plikow")
    String fileSystem;
    @Column(nullable = false, name = "wolna_przestrzen")
    BigInteger freeSpace;
    @Column(nullable = false, name = "calkowita_przestrzen")
    BigInteger allSpace;
    @Column(nullable = false, name = "nazwa_woluminu")
    String volumeName;
    @Column(nullable = false, name = "identyfikator_woluminu")
    String idVolume;

}
