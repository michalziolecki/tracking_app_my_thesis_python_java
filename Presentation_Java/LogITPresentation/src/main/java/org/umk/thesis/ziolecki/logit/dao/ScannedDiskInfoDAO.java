package org.umk.thesis.ziolecki.logit.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.umk.thesis.ziolecki.logit.entity.ScannedDiskInfo;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.List;

@Repository
public interface ScannedDiskInfoDAO extends CrudRepository<ScannedDiskInfo, Integer> {
    List<ScannedDiskInfo> findByUser(UserKeylogger userKeylogger);
}
