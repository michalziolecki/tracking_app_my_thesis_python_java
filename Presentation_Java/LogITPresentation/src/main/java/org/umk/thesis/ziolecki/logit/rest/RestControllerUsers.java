package org.umk.thesis.ziolecki.logit.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;
import org.umk.thesis.ziolecki.logit.service.ServiceUserKeyloggerDAO;

@Controller
public class RestControllerUsers {

    private ServiceUserKeyloggerDAO serviceUserKeyloggerDAO;

    @Autowired
    public RestControllerUsers(ServiceUserKeyloggerDAO serviceUserKeyloggerDAO){
        this.serviceUserKeyloggerDAO = serviceUserKeyloggerDAO;
    }

    @RequestMapping(value = {"/users"}, method = RequestMethod.GET)
    public String getAllUsers(Model model) {
        model.addAttribute("users", this.serviceUserKeyloggerDAO.getAllUsers());
        return "users";
    }

    @RequestMapping(value = {"/showUser"}, method = RequestMethod.GET)
    public String getMainWebsite(Model model, @RequestParam Integer id) {
        model.addAttribute("user", this.serviceUserKeyloggerDAO.getUserById(id));
        return "userInterface";
    }
}
