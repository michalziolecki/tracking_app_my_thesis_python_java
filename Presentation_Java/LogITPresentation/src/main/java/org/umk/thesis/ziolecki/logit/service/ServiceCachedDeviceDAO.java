package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.CachedDeviceDAO;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;
import org.umk.thesis.ziolecki.logit.entity.CachedDevice;
import org.umk.thesis.ziolecki.logit.entity.KeyloggerMsg;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceCachedDeviceDAO {

    private CachedDeviceDAO cachedDeviceDAO;
    private UserKeyloggerDAO userKeyloggerDAO;

    @Autowired
    public ServiceCachedDeviceDAO(CachedDeviceDAO cachedDeviceDAO,
                                  UserKeyloggerDAO userKeyloggerDAO){
        this.cachedDeviceDAO = cachedDeviceDAO;
        this.userKeyloggerDAO = userKeyloggerDAO;
    }

    public List<CachedDevice> getAllCachedDevices(){
        List<CachedDevice> cachedDevices = new ArrayList<>();
        this.cachedDeviceDAO.findAll().forEach(cachedDevices::add);
        return cachedDevices;
    }

    public List<CachedDevice> getCachedDevicesByUserId(Integer id){
        UserKeylogger userKeylogger = this.userKeyloggerDAO.findById(id).get();
        return this.cachedDeviceDAO.findByUser(userKeylogger);
    }
}
