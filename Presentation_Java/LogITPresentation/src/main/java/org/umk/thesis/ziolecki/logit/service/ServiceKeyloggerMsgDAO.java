package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.KeyloggerMsgDAO;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;
import org.umk.thesis.ziolecki.logit.entity.KeyloggerMsg;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceKeyloggerMsgDAO {

    private KeyloggerMsgDAO keyloggerMsgDAO;
    private UserKeyloggerDAO userKeyloggerDAO;

    @Autowired
    public ServiceKeyloggerMsgDAO(KeyloggerMsgDAO keyloggerMsgDAO,
                                  UserKeyloggerDAO userKeyloggerDAO){
        this.keyloggerMsgDAO = keyloggerMsgDAO;
        this.userKeyloggerDAO = userKeyloggerDAO;
    }

    public List<KeyloggerMsg> getAllKeyloggerMsg(){
        List<KeyloggerMsg> keyloggerMsgs = new ArrayList<>();
        this.keyloggerMsgDAO.findAll().forEach(keyloggerMsg -> keyloggerMsgs.add(keyloggerMsg));
        return keyloggerMsgs;
    }

    public List<KeyloggerMsg> getKeyloggerMsgByUserId(Integer id){
        UserKeylogger userKeylogger = this.userKeyloggerDAO.findById(id).get();
        return this.keyloggerMsgDAO.findAllByUser(userKeylogger);
    }
}
