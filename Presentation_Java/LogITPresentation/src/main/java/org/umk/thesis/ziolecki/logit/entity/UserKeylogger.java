package org.umk.thesis.ziolecki.logit.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "uzytkownicy_keylogger")
public class UserKeylogger {
    // columns
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, name = "nazwa_komputera")
    private String computerName;
    @Column(nullable = false, name = "nazwa_uzytkownika_na_komputerze")
    private String userName;
    @Column(name = "opis")
    private String description;

    // references
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    BlockUSBSettings blockUSBSettings;
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    KeyloggerSettings keyloggerSettings;
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    TrackUSBSettings trackUSBSettings;
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    TrackDiscDriverSettings trackDiscDriverSettings;
    @OneToMany(targetEntity = UserKeylogger.class)
    @PrimaryKeyJoinColumn
    private Set<KeyloggerMsg> keyloggerMsg;
    @OneToMany(targetEntity = UserKeylogger.class)
    @PrimaryKeyJoinColumn
    private Set<KeyloggerFilter> keyloggerFilters;
    @OneToMany(targetEntity = CachedDevice.class)
    @PrimaryKeyJoinColumn
    private Set<CachedDevice> cachedDevices;
    @OneToMany(targetEntity = RemoteDiskInfo.class)
    @PrimaryKeyJoinColumn
    private Set<RemoteDiskInfo> remoteDiskInfoSet;
    @OneToMany(targetEntity = RemoteDiskInfo.class)
    @PrimaryKeyJoinColumn
    private Set<UntrackedPartition> untrackedPartitions;
}
