package org.umk.thesis.ziolecki.logit.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "keylogger_wiadomosci")
public class KeyloggerMsg {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(nullable = false, name = "id_uzytkownicy_keylogger")
    private UserKeylogger user;
    @Column(nullable = false, name = "wiadomosc")
    private String trackMsg;
    @Column(nullable = false, name = "kiedy_zebrano_wiadomosc")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;

}
