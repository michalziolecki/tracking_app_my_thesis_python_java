package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.ScannedDiskInfoDAO;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;
import org.umk.thesis.ziolecki.logit.entity.ScannedDiskInfo;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceScannedDiskInfoDAO {

    private UserKeyloggerDAO userKeyloggerDAO;
    private ScannedDiskInfoDAO scannedDiskInfoDAO;

    @Autowired
    public ServiceScannedDiskInfoDAO(UserKeyloggerDAO userKeyloggerDAO,
                                     ScannedDiskInfoDAO scannedDiskInfoDAO){
        this.userKeyloggerDAO = userKeyloggerDAO;
        this.scannedDiskInfoDAO = scannedDiskInfoDAO;
    }

    public List<ScannedDiskInfo> getAllScannedDiskInfo(){
        List<ScannedDiskInfo> scannedDiskInfo = new ArrayList<>();
        this.scannedDiskInfoDAO.findAll().forEach(scannedDiskInfo::add);
        return scannedDiskInfo;
    }

    public List<ScannedDiskInfo> getScannedDiskInfoByUserId(Integer id){
        UserKeylogger userKeylogger = this.userKeyloggerDAO.findById(id).get();
        return this.scannedDiskInfoDAO.findByUser(userKeylogger);
    }

}
