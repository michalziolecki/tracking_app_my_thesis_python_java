package org.umk.thesis.ziolecki.logit.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.umk.thesis.ziolecki.logit.entity.KeyloggerMsg;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.List;

@Repository
public interface KeyloggerMsgDAO extends CrudRepository<KeyloggerMsg, Integer> {
    List<KeyloggerMsg> findAllByUser(UserKeylogger userKeylogger);
}
