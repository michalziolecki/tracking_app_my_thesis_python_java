package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.RemoteDiskInfoDAO;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;
import org.umk.thesis.ziolecki.logit.entity.RemoteDiskInfo;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceRemoteDiskInfoDAO {

    private RemoteDiskInfoDAO remoteDiskInfoDAO;
    private UserKeyloggerDAO userKeyloggerDAO;

    @Autowired
    public ServiceRemoteDiskInfoDAO(RemoteDiskInfoDAO remoteDiskInfoDAO,
                                  UserKeyloggerDAO userKeyloggerDAO){
        this.remoteDiskInfoDAO = remoteDiskInfoDAO;
        this.userKeyloggerDAO = userKeyloggerDAO;
    }

    public List<RemoteDiskInfo> getAllRemoteDiskInfo(){
        List<RemoteDiskInfo> cachedDevices = new ArrayList<>();
        this.remoteDiskInfoDAO.findAll().forEach(cachedDevices::add);
        return cachedDevices;
    }

    public List<RemoteDiskInfo> getCachedDevicesByUserId(Integer id){
        UserKeylogger userKeylogger = this.userKeyloggerDAO.findById(id).get();
        return this.remoteDiskInfoDAO.findByUser(userKeylogger);
    }
}
