package org.umk.thesis.ziolecki.logit.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.umk.thesis.ziolecki.logit.entity.KeyloggerFilter;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.List;

@Repository
public interface KeyloggerFilterDAO extends CrudRepository<KeyloggerFilter, Integer> {
    List<KeyloggerFilter> findByUser(UserKeylogger userKeylogger);
}
