package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.TrackUSBSettingsDAO;
import org.umk.thesis.ziolecki.logit.entity.TrackUSBSettings;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceTrackUSBSettingsDAO {

    private TrackUSBSettingsDAO trackUSBSettingsDAO;
    private UserKeyloggerDAO userKeyloggerDAO;

    @Autowired
    public ServiceTrackUSBSettingsDAO(TrackUSBSettingsDAO trackUSBSettingsDAO,
                                      UserKeyloggerDAO userKeyloggerDAO){
        this.trackUSBSettingsDAO = trackUSBSettingsDAO;
        this.userKeyloggerDAO = userKeyloggerDAO;
    }


    public TrackUSBSettings getUSBSettingsById(int id){
        return this.trackUSBSettingsDAO.findById(id).get();
    }

    public TrackUSBSettings getUSBSettingsByUserId(int id){
        UserKeylogger userKeylogger = this.userKeyloggerDAO.findById(id).get();
        return this.trackUSBSettingsDAO.findByUser(userKeylogger);
    }

    public TrackUSBSettings getUSBSettingsByUser(UserKeylogger userKeylogger){
        return this.trackUSBSettingsDAO.findByUser(userKeylogger);
    }

    public List<TrackUSBSettings> getAllUSBSettings(){
        List<TrackUSBSettings> allTrackUSBSettings = new ArrayList<>();
        this.trackUSBSettingsDAO.findAll().forEach(allTrackUSBSettings::add);
        return allTrackUSBSettings;
    }

    public TrackUSBSettings updateUSBSettings(TrackUSBSettings trackUSBSettings){
        return this.trackUSBSettingsDAO.save(trackUSBSettings);
    }
}
