package org.umk.thesis.ziolecki.logit.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "urzadzenia_sledzenie")
public class CachedDevice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(nullable = false, name = "id_uzytkownicy_keylogger")
    private UserKeylogger user;
    @Column(nullable = false, name = "data_operacji")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;
    @Column(nullable = false, name = "rozpoznany_jako")
    String recognizeInfo;
    @Column(nullable = false, name = "status_operacji")
    String operationStatus;
    @Column(nullable = false, name = "nazwa_urzadzenia")
    String deviceName;
    @Column(nullable = false, name = "opis")
    String description;
    @Column(nullable = false, name = "producent")
    String inventor;
    @Column(nullable = false, name = "id_urzadzenia")
    String deviceId;
    @Column(nullable = false, name = "guid_urzadzenia")
    String guid;
    @Column(nullable = false, name = "typ_urzadzenia")
    String deviceType;
}
