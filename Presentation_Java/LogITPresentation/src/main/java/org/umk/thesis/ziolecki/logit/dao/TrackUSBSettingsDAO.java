package org.umk.thesis.ziolecki.logit.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.umk.thesis.ziolecki.logit.entity.TrackUSBSettings;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;


@Repository
public interface TrackUSBSettingsDAO extends CrudRepository<TrackUSBSettings, Integer> {
    TrackUSBSettings findByUser(UserKeylogger userKeylogger);
}
