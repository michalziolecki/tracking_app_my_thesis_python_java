package org.umk.thesis.ziolecki.logit.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.umk.thesis.ziolecki.logit.entity.BlockUSBSettings;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

@Repository
public interface BlockUSBSettingsDAO extends CrudRepository <BlockUSBSettings, Integer> {
    BlockUSBSettings findByUser(UserKeylogger userKeylogger);
}
