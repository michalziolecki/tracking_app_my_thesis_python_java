package org.umk.thesis.ziolecki.logit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.umk.thesis.ziolecki.logit.dao.KeyloggerFilterDAO;
import org.umk.thesis.ziolecki.logit.dao.UserKeyloggerDAO;
import org.umk.thesis.ziolecki.logit.entity.KeyloggerFilter;
import org.umk.thesis.ziolecki.logit.entity.UserKeylogger;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceKeyloggerFilterDAO {

    private KeyloggerFilterDAO keyloggerFilterDAO;
    private UserKeyloggerDAO userKeyloggerDAO;

    @Autowired
    public ServiceKeyloggerFilterDAO(KeyloggerFilterDAO keyloggerFilterDAO, UserKeyloggerDAO userKeyloggerDAO){
        this.keyloggerFilterDAO = keyloggerFilterDAO;
        this.userKeyloggerDAO = userKeyloggerDAO;
    }

    public List<KeyloggerFilter> getAllKeylogFilter(){
        List<KeyloggerFilter> keyloggerFilters = new ArrayList<>();
        this.keyloggerFilterDAO.findAll().forEach(keyloggerFilter -> keyloggerFilters.add(keyloggerFilter));
        return keyloggerFilters;
    }

    public List<KeyloggerFilter> getUserKeylogFilterByUserId(int id){
        UserKeylogger userKeylogger = this.userKeyloggerDAO.findById(id).get();
        return this.keyloggerFilterDAO.findByUser(userKeylogger);
    }

    public KeyloggerFilter addKeylogFilter(KeyloggerFilter keyloggerFilter){
        return this.keyloggerFilterDAO.save(keyloggerFilter);
    }

    public KeyloggerFilter updateKeylogFilter(KeyloggerFilter keyloggerFilter){
        return this.keyloggerFilterDAO.save(keyloggerFilter);
    }

    public KeyloggerFilter getKeyloggerFilterById(Integer id){
        return this.keyloggerFilterDAO.findById(id).get();
    }

    public void daleteKeylogFilter(KeyloggerFilter keyloggerFilter){
        this.keyloggerFilterDAO.delete(keyloggerFilter);
    }
}
