package org.umk.thesis.ziolecki.logit.entity;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "sledzenie_napedu_plyt")
public class TrackDiscDriverSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne(targetEntity = UserKeylogger.class, cascade = CascadeType.ALL)
    @JoinColumn(nullable = false, name = "id_uzytkownicy_keylogger", referencedColumnName = "id")
    private UserKeylogger user;
    @Column(nullable = false, name = "czy_sledzic", columnDefinition = "SMALLINT")
    @Type(type = "org.hibernate.type.ShortType")
    private Short track;
    @Column(nullable = false, name = "czy_skanowac", columnDefinition = "SMALLINT")
    @Type(type = "org.hibernate.type.ShortType")
    private Short scan;
}
