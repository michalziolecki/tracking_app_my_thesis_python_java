package org.umk.thesis.ziolecki.logit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogITApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogITApplication.class, args);
    }

}
