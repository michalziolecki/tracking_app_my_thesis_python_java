package org.umk.thesis.ziolecki.logit.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "skanowanie_dyskow_zdalnych")
public class ScannedDiskInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(nullable = false, name = "id_uzytkownicy_keylogger")
    private UserKeylogger user;
    @Column(nullable = false, name = "id_dysku")
    private Integer idDisc;
    @Column(nullable = false, name = "nazwa_dysku")
    private String diskName;
    @Column(nullable = false, name = "data_operacji")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;
    @Column(nullable = false, name = "nazwa_pliku")
    private String fileName;
    @Column(nullable = false, name = "sciezka_pliku")
    private String filePath;
    @Column(nullable = false, name = "typ_pliku")
    private String fileType;
    @Column(nullable = false, name = "jest_binarny", columnDefinition = "SMALLINT")
    @Type(type = "org.hibernate.type.ShortType")
    private Short isBinary;
    @Column(nullable = false, name = "rozszerzenie")
    private String extension;
    @Column(nullable = false, name = "rozmiar")
    BigInteger sizeFile;
}
