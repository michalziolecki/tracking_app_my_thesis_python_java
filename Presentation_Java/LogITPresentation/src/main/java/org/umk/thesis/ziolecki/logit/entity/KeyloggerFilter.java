package org.umk.thesis.ziolecki.logit.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "keylogger_filtr")
public class KeyloggerFilter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(nullable = false, name = "id_uzytkownicy_keylogger")
    private UserKeylogger user;
    @Column(nullable = false, name = "filtr")
    private String trackMsg;
}
