# -*- mode: python -*-

block_cipher = None


a = Analysis(['ConfigApp.py'],
             pathex=['C:\\Users\\Admin\\Desktop\\Informatyka_3_rok\\LICENCJAT\\Ziolecki_thesis\\Cze��_programistyczna\\thesis_umk_ziolecki\\Standalone_Python_app\\LogIT'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='ConfigApp',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='ConfigApp')
