import os
# This file has main parameters used by application

CONFIG_FILE = 'Config.ini'
CONFIG_PATH = './'
CONFIG_LOG_PATH = '{}/{}'.format(os.getenv('APPDATA'), 'LogIT/logs')
CONFIG_LOG_FILE = 'Config.log'
KEY_TO_THE_CIPHER = b'iewOCzeFWvqF5MZRhFzG-nhqK1F0bO6MNl2knTidSKw='