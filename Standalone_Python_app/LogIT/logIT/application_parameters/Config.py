from configparser import ConfigParser
from logIT.application_parameters.application_param import *
from logIT.application_parameters.Parameters import Parameters
import json
import datetime
from logIT.application_parameters.application_param import KEY_TO_THE_CIPHER
from logIT.encryption.encryption import Encryption


class Config:
    def __init__(self, path=CONFIG_PATH, file=CONFIG_FILE):
        self.file = file
        self.path = path
        self.path_log = CONFIG_LOG_PATH
        self.file_log = CONFIG_LOG_FILE
        self.section_database = 'DataBase'
        self.section_files = 'Files'
        self.section_usb = 'USB'
        self.section_keylogger = "Keylogger"
        self.cipher = Encryption(KEY_TO_THE_CIPHER)

    def _path_and_file(self):
        return "{0}{1}".format(self.path, self.file)

    def _path_and_file_log(self):
        return "{0}/{1}".format(self.path_log, self.file_log)

    def _file_exists(self):
        return os.path.exists(self._path_and_file())

    def _path_log_exists(self):
        return os.path.isdir(self.path_log)

    def _replace_appdata(self, path):
        return str(path).replace('%appdata%', os.getenv('APPDATA'))

    def _percent_to_double_percent(self, text) -> str:
        text_new = ''
        for char in text:
            if (char == '%'):
                text_new += char
                text_new += '%'
            else:
                text_new += char
        return text_new

    def _double_percent_to_percent(self, text) -> str:
        text_new = str(text)
        return text_new.replace('%%', '%')

    def create_file(self, if_config_app):
        config = ConfigParser()
        # DataBase
        config.add_section(self.section_database)
        config.set(self.section_database, 'DB_LOGIN', Parameters.DB_LOGIN)
        config.set(self.section_database, 'DB_PASSWORD', self.cipher.encrtpted(Parameters.DB_PASSWORD))
        config.set(self.section_database, 'DB_ADDRESS', Parameters.DB_ADDRESS)
        config.set(self.section_database, 'DB_PORT', Parameters.DB_PORT)
        config.set(self.section_database, 'DB_NAME', Parameters.DB_NAME)
        # Files
        config.add_section(self.section_files)
        if not if_config_app:
            config.set(self.section_files, 'LOG_PATH', self._percent_to_double_percent(Parameters.LOG_PATH))
        else:
            config.set(self.section_files, 'LOG_PATH', Parameters.LOG_PATH)
        config.set(self.section_files, 'LOG_FILE_NAME', Parameters.LOG_FILE_NAME)
        if not if_config_app:
            config.set(self.section_files, 'KEYLOG_FILE_PATH ',
                       self._percent_to_double_percent(Parameters.KEYLOG_FILE_PATH))
        else:
            config.set(self.section_files, 'KEYLOG_FILE_PATH ', Parameters.KEYLOG_FILE_PATH)
        config.set(self.section_files, 'KEYLOG_FILE_NAME', Parameters.KEYLOG_FILE_NAME)
        config.set(self.section_files, 'USB_DEVICES_INFO_FILE_NAME', Parameters.USB_DEVICES_INFO_FILE_NAME)
        config.set(self.section_files, 'REMOTE_MEMORY_INFO_FILE_NAME', Parameters.REMOTE_MEMORY_INFO_FILE_NAME)
        config.set(self.section_files, 'REMOTE_MEMORY_FILES_FILE_NAME', Parameters.REMOTE_MEMORY_FILES_FILE_NAME)
        # USB
        config.add_section(self.section_usb)
        if not if_config_app:
            config.set(self.section_usb, 'UNTRACKED_PARTITIONS_IN_OFFLINE',
                       json.dumps(Parameters.UNTRACKED_PARTITIONS_IN_OFFLINE))
        else:
            config.set(self.section_usb, 'UNTRACKED_PARTITIONS_IN_OFFLINE', Parameters.UNTRACKED_PARTITIONS_IN_OFFLINE)
        config.set(self.section_usb, 'USB_DEVICES_PERIODIC_TIME', str(Parameters.USB_DEVICES_PERIODIC_TIME))
        config.set(self.section_usb, 'DISK_DRIVERS_SCAN_PERIODIC_TIME', str(Parameters.DISK_DRIVERS_SCAN_PERIODIC_TIME))
        config.set(self.section_usb, 'DB_DEVICE_PERIODIC_CONNECTION_TIME',
                   str(Parameters.DB_DEVICE_PERIODIC_CONNECTION_TIME))
        # Keylogger
        config.add_section(self.section_keylogger)
        config.set(self.section_keylogger, 'DB_KEYLOG_PERIODIC_CONNECTION_TIME',
                   str(Parameters.DB_KEYLOG_PERIODIC_CONNECTION_TIME))
        config.set(self.section_keylogger, 'FILE_KEYLOG_PERIODIC_WRITING_TIME',
                   str(Parameters.FILE_KEYLOG_PERIODIC_WRITING_TIME))
        if not if_config_app:
            config.set(self.section_keylogger, 'DB_KEYLOG_DATETIME_FORMAT',
                       self._percent_to_double_percent(Parameters.DB_KEYLOG_DATETIME_FORMAT))
        else:
            config.set(self.section_keylogger, 'DB_KEYLOG_DATETIME_FORMAT', Parameters.DB_KEYLOG_DATETIME_FORMAT)

        with open(self._path_and_file(), 'w') as configfile:
            config.write(configfile)

    def _assign_variables(self):
        config = ConfigParser()
        config.read(self._path_and_file())
        if config.has_option(self.section_keylogger, 'DB_KEYLOG_PERIODIC_CONNECTION_TIME'):
            Parameters.DB_KEYLOG_PERIODIC_CONNECTION_TIME = config.getint(self.section_keylogger,
                                                                          'DB_KEYLOG_PERIODIC_CONNECTION_TIME')
        if config.has_option(self.section_keylogger, 'FILE_KEYLOG_PERIODIC_WRITING_TIME'):
            Parameters.FILE_KEYLOG_PERIODIC_WRITING_TIME = config.getint(self.section_keylogger,
                                                                         'FILE_KEYLOG_PERIODIC_WRITING_TIME')
        if config.has_option(self.section_keylogger, 'DB_KEYLOG_DATETIME_FORMAT'):
            Parameters.DB_KEYLOG_DATETIME_FORMAT = self._double_percent_to_percent(
                config.get(self.section_keylogger, 'DB_KEYLOG_DATETIME_FORMAT'))
        if config.has_option(self.section_database, 'DB_LOGIN'):
            Parameters.DB_LOGIN = config.get(self.section_database, 'DB_LOGIN')
        if config.has_option(self.section_database, 'DB_PASSWORD'):
            Parameters.DB_PASSWORD = self.cipher.decrypted(config.get(self.section_database, 'DB_PASSWORD'))
        if config.has_option(self.section_database, 'DB_ADDRESS'):
            Parameters.DB_ADDRESS = config.get(self.section_database, 'DB_ADDRESS')
        if config.has_option(self.section_database, 'DB_PORT'):
            Parameters.DB_PORT = config.get(self.section_database, 'DB_PORT')
        if config.has_option(self.section_database, 'DB_NAME'):
            Parameters.DB_NAME = config.get(self.section_database, 'DB_NAME')
        if config.has_option(self.section_files, 'LOG_PATH'):
            Parameters.LOG_PATH = self._replace_appdata(
                self._double_percent_to_percent(config.get(self.section_files, 'LOG_PATH')))
        if config.has_option(self.section_files, 'LOG_FILE_NAME'):
            Parameters.LOG_FILE_NAME = config.get(self.section_files, 'LOG_FILE_NAME')
        if config.has_option(self.section_files, 'KEYLOG_FILE_PATH'):
            Parameters.KEYLOG_FILE_PATH = self._replace_appdata(
                self._double_percent_to_percent(config.get(self.section_files, 'KEYLOG_FILE_PATH')))
        if config.has_option(self.section_files, 'KEYLOG_FILE_NAME'):
            Parameters.KEYLOG_FILE_NAME = config.get(self.section_files, 'KEYLOG_FILE_NAME')
        if config.has_option(self.section_files, 'USB_DEVICES_INFO_FILE_NAME'):
            Parameters.USB_DEVICES_INFO_FILE_NAME = config.get(self.section_files, 'USB_DEVICES_INFO_FILE_NAME')
        if config.has_option(self.section_files, 'REMOTE_MEMORY_INFO_FILE_NAME'):
            Parameters.REMOTE_MEMORY_INFO_FILE_NAME = config.get(self.section_files, 'REMOTE_MEMORY_INFO_FILE_NAME')
        if config.has_option(self.section_files, 'REMOTE_MEMORY_FILES_FILE_NAME'):
            Parameters.REMOTE_MEMORY_FILES_FILE_NAME = config.get(self.section_files, 'REMOTE_MEMORY_FILES_FILE_NAME')
        if config.has_option(self.section_usb, 'USB_DEVICES_PERIODIC_TIME'):
            Parameters.USB_DEVICES_PERIODIC_TIME = config.getint(self.section_usb, 'USB_DEVICES_PERIODIC_TIME')
        if config.has_option(self.section_usb, 'DISK_DRIVERS_SCAN_PERIODIC_TIME'):
            Parameters.DISK_DRIVERS_SCAN_PERIODIC_TIME = config.getint(self.section_usb,
                                                                       'DISK_DRIVERS_SCAN_PERIODIC_TIME')
        if config.has_option(self.section_usb, 'DB_DEVICE_PERIODIC_CONNECTION_TIME'):
            Parameters.DB_DEVICE_PERIODIC_CONNECTION_TIME = config.getint(self.section_usb,
                                                                          'DB_DEVICE_PERIODIC_CONNECTION_TIME')
        if config.has_option(self.section_usb, 'UNTRACKED_PARTITIONS_IN_OFFLINE'):
            Parameters.UNTRACKED_PARTITIONS_IN_OFFLINE = json.loads(
                config.get(self.section_usb, 'UNTRACKED_PARTITIONS_IN_OFFLINE'))

    def _create_log(self, text):
        if not self._path_log_exists():
            os.makedirs(self.path_log)
        file_log = open(self._path_and_file_log(), 'a')
        file_log.write('{}:     {}\n'.format(datetime.datetime.now(), text))
        file_log.close()

    def _variable_substitutions(self):
        Parameters.KEYLOG_FILE_PATH = self._replace_appdata(
            self._double_percent_to_percent(Parameters.KEYLOG_FILE_PATH))
        Parameters.LOG_PATH = self._replace_appdata(self._double_percent_to_percent(Parameters.LOG_PATH))

    def run(self, if_config_app):
        try:
            if not self._file_exists():
                self.create_file(if_config_app)
                self._variable_substitutions()
            else:
                self._assign_variables()
                self._variable_substitutions()
        except Exception as ex:
            try:
                self._variable_substitutions()
                self._create_log(ex)
            except Exception as e:
                print('Exceptions while writing log. Info: {}, Error: {}'.format(ex, e))
