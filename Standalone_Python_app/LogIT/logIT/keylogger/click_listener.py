from logIT.keylogger.sentence_analyzer import SentenceAnalyzer

from pynput import mouse


class ClickListener:

    def __init__(self, analyzer_obj):
        self._analyzer = analyzer_obj
        self._listener = mouse.Listener(on_click=self._on_click_event,
                                        on_move=self._on_move_event, on_scroll=self._on_scroll_event)

    @property
    def analyzer(self) -> SentenceAnalyzer:
        return self._analyzer

    def start_listening(self):
        self._analyzer._writing_thread_status = True
        self._listener.start()
        # self._listener.join()

    def stop_listening(self):
        self._listener.stop()
        self._analyzer._writing_thread_status = False

    def _on_move_event(self, x, y):
        # method to check position of mouse X and Y
        # e.g print('Pointer moved to {0}'.format((x, y)))
        pass

    def _on_click_event(self, x, y, button: mouse.Button, pressed: bool):
        if not pressed and self.analyzer.get_length_of_sentence() > 0:
            self.analyzer.add_sentence_to_list()

    def _on_scroll_event(self, x, y, dx, dy):
        # method to check changing of position by scrolling
        # e.g print('Scrolled {0}'.format((x, y)))
        pass
