class KeyLogEntity:

    def __init__(self, keylog_entity_config):
        self._nick_name = keylog_entity_config['nick_name']
        self._host_name = keylog_entity_config['host_name']
        self._date_time_info = keylog_entity_config['date_time']
        self._sentence = keylog_entity_config['sentence']
        self._mac_addr = keylog_entity_config['mac_addr']

    @property
    def nick_name(self) -> str:
        return self._nick_name

    @property
    def host_name(self) -> str:
        return self._host_name

    @property
    def date_time_info(self) -> str:
        return self._date_time_info

    @property
    def sentence(self) -> str:
        return self._sentence

    @property
    def mac_addr(self) -> str:
        return self._mac_addr

    def get_dict_of_entity(self) -> dict:
        date_sentence_dict = {
            'date_time_info': self.date_time_info,
            'sentence': self.sentence
        }
        return date_sentence_dict

    def get_full_dict_of_entity(self) -> dict:
        return self.__dict__
