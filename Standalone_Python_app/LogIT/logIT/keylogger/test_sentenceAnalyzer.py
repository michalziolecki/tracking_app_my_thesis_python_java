from unittest import TestCase

from logIT.keylogger.sentence_analyzer import SentenceAnalyzer


class TestSentenceAnalyzer(TestCase):

    def test__check_filter_correction(self):
        # input
        first_test_phrase = 'www.facebook.com'
        second_test_phrase = 'www.google.com'
        third_test_phrase = 'Ala Ma Kota'
        analyzer = SentenceAnalyzer()
        analyzer.keylog_filter = ['facebook.com', 'google']

        # test
        self.assertTrue(analyzer._check_filter_correction(first_test_phrase))
        self.assertTrue(analyzer._check_filter_correction(second_test_phrase))
        self.assertFalse(analyzer._check_filter_correction(third_test_phrase))
