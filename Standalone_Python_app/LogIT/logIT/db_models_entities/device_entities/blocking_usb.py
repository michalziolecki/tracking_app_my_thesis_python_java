from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, Boolean
from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from logIT.develop_log.dev_log import Logger
from sqlalchemy import update
from copy import copy


class BlockUSB(Base, AbstractEntityDAO):
    __tablename__ = 'blokowanie_usb'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False,
                               name='id_uzytkownicy_keylogger')
    block_port_status = Column(Boolean, nullable=False, name='blokuj_porty')  # in DB this is smallint

    def __init__(self, id_user: int):
        super().__init__()
        self.id_user_keylogger = id_user
        self.block_port_status_in_db = None
        self.block_port_status = True
        self._logger = Logger.create_to_file_logger(logging_name='BlockUSB')

    def set_id_user_keylogger(self, id_user: int):
        self.id_user_keylogger = id_user

    def set_block_port_status(self, block_port_status):
        self.block_port_status = block_port_status

    def save_block_port_status_in_db(self):
        self.create_new_session()
        self.save_entity(self)
        self.do_commit_session()
        self.close_session()

    def update_block_port_status_in_db(self):
        self.create_new_session()
        self.session.query(BlockUSB).filter(BlockUSB.id_user_keylogger == self.id_user_keylogger) \
            .update({'block_port_status': self.block_port_status})
        self.do_commit_session()
        self.close_session()

    def get_block_port_status_from_db(self) -> bool:

        self.create_new_session()
        block_port_snapshot: list = self.session.query(BlockUSB) \
            .filter(BlockUSB.id_user_keylogger == self.id_user_keylogger).all()

        if len(block_port_snapshot) == 0:
            self.save_entity(self)
            new_block_port_snapshot: list = self.session.query(BlockUSB) \
                .filter(BlockUSB.id_user_keylogger == self.id_user_keylogger).all()

            if new_block_port_snapshot is not None and len(new_block_port_snapshot) > 0:
                self.block_port_status_in_db = copy(new_block_port_snapshot[0].block_port_status)

        elif len(block_port_snapshot) == 1:
            self.block_port_status_in_db = copy(block_port_snapshot[0].block_port_status)

        else:
            self._logger.warning(
                msg='Duplicated block post status, application use first but, fix this problem immediately!!! ')
            self.block_port_status_in_db = copy(block_port_snapshot[0].block_port_status)

        self.do_commit_session()
        self.close_session()

        if self.block_port_status_in_db is None:
            self._logger.error(msg='User does\'nt created in  DB by method: '
                                   + self.get_block_port_status_from_db.__name__ + ' method! ')
            raise NotImplementedError(
                'User does\'nt created in  DB by method: ' + self.get_block_port_status_from_db.__name__ + ' method! ')

        return self.block_port_status_in_db
