from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer
from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from logIT.develop_log.dev_log import Logger
from copy import copy


class UntrackedPartitions(Base, AbstractEntityDAO):
    __tablename__ = 'niesledzone_partycje'

    id = Column(Integer, primary_key=True, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False,
                               name='id_uzytkownicy_keylogger')
    untracked_partition_disk = Column(String(3), nullable=False, name='nazwa_partycji_dysku')

    def __init__(self, id_user: int):
        super().__init__()
        self.id_user_keylogger = id_user
        self.untracked_partition_disk = None
        self._logger = Logger.create_to_file_logger(logging_name='UntrackedPartitions')

    def set_partition_disk_str(self, partition_disk: str):
        self.untracked_partition_disk = partition_disk

    def save_partition_disk_in_db(self):
        self.create_new_session()
        self.save_entity(self)
        self.do_commit_session()
        self.close_session()

    def save_partition_disks_list_in_db(self, partition_disks: list):

        for partition_disk in partition_disks:
            entity = UntrackedPartitions(self.id_user_keylogger)
            entity.untracked_partition_disk = partition_disk
            entity.save_partition_disk_in_db()

    def get_all_partition_disks_for_user_from_db(self) -> list:
        list_of_partitions = []
        self.create_new_session()
        partitions_snapshot: list = self.session.query(UntrackedPartitions) \
            .filter(UntrackedPartitions.id_user_keylogger == self.id_user_keylogger).all()
        if partitions_snapshot is not None and len(partitions_snapshot) > 0:
            for entity in partitions_snapshot:
                untracked_partition_disk = copy(entity.untracked_partition_disk)
                list_of_partitions.append(untracked_partition_disk)
        self.do_commit_session()
        self.close_session()

        return list_of_partitions
