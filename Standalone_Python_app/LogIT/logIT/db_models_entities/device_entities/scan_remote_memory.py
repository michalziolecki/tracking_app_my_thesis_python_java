from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer
from sqlalchemy.orm import relationship
from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from sqlalchemy import String, Integer, DateTime, Boolean
from sqlalchemy import and_, or_, not_
from logIT.usb_module.classes.scan_class import ScanFileEntity
from logIT.develop_log.dev_log import Logger
from copy import copy
from datetime import datetime
from logIT.application_parameters.Parameters import Parameters


class ScanRemoteMemoryEntity(Base, AbstractEntityDAO):
    __tablename__ = 'skanowanie_dyskow_zdalnych'

    id = Column(Integer, primary_key=True, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False,
                               name='id_uzytkownicy_keylogger')
    id_disk = Column(Integer, ForeignKey('informacje_dyski_zdalne.id'), nullable=False,
                     name='id_dysku')
    disk_name = Column(String(3), nullable=False, name='nazwa_dysku')
    date_time_info = Column(DateTime, nullable=False, name='data_operacji')  # timestamp in DB
    file_name = Column(String(250), nullable=False, name='nazwa_pliku')
    file_path = Column(String(512), nullable=False, name='sciezka_pliku')
    file_type = Column(String(100), nullable=False, name='typ_pliku')  # DO POPRAWY !! na wieksyz zna nawet 100
    is_binary = Column(Integer, nullable=False, name='jest_binarny')
    extension = Column(String(10), nullable=False, name='rozszerzenie')  # LITERÓWKA !!!
    file_size = Column(Integer, nullable=False, name='rozmiar')

    def __init__(self, _entity, id_user: int = None, disk_id: int = None):
        super().__init__()
        self.id_user_keylogger = id_user
        self.id_disk = disk_id
        if isinstance(_entity, ScanFileEntity):
            self.date_time_info = datetime.strptime(_entity.date_time_info, Parameters.DB_KEYLOG_DATETIME_FORMAT)
            self.disk_name = _entity.scanned_disk
            self.file_name = _entity.file_name
            self.file_path = _entity.path_name
            self.file_type = _entity.file_type
            self.is_binary = int(_entity.is_binary)
            self.extension = _entity.file_extension
            self.file_size = round(int(_entity.file_size) / 1000)
        elif isinstance(_entity, dict):
            self.date_time_info = datetime.strptime(_entity['_date_time_info'], Parameters.DB_KEYLOG_DATETIME_FORMAT)
            self.disk_name = _entity['_scanned_disk']
            self.file_name = _entity['_file_name']
            self.file_path = _entity['_path_name']
            self.file_type = _entity['_file_type']
            self.is_binary = int(_entity['_is_binary'])
            self.extension = _entity['_file_extension']
            self.file_size = round(int(_entity['_file_size']) / 1000)

        if len(self.file_path) > 500:
            self.file_path = self.file_path[:500] + '...'
        self._logger = Logger.create_to_file_logger(logging_name='ScanRemoteMemoryEntity')

    def set_id_user_keylogger(self, id_user: int):
        self.id_user_keylogger = id_user

    def set_disk_id(self, disk_id: int):
        self.id_disk = disk_id

    def save_remote_memory_info_in_db(self):
        self.create_new_session()
        self.save_entity(self)
        self.do_commit_session()
        self.close_session()
