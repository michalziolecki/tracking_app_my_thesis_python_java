from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer
from sqlalchemy.orm import relationship
from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from sqlalchemy import String, Integer, DateTime
from sqlalchemy import and_, or_, not_
from logIT.usb_module.file_operations.usb_json_entity import USBInsertInfoEntity
from logIT.application_parameters.Parameters import Parameters
from logIT.develop_log.dev_log import Logger
from copy import copy
from datetime import datetime


class DeviceInfoEntity(Base, AbstractEntityDAO):
    __tablename__ = 'urzadzenia_sledzenie'

    id = Column(Integer, primary_key=True, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False,
                               name='id_uzytkownicy_keylogger')
    date_time_info = Column(DateTime, nullable=False, name='data_operacji')  # timestamp in DB
    dev_recognize_as = Column(String(50), nullable=False, name='rozpoznany_jako')
    insert_status = Column(String(3), nullable=False, name='status_operacji')
    dev_name = Column(String(250), nullable=False, name='nazwa_urzadzenia')
    description = Column(String(250), nullable=False, name='opis')
    manufacturer = Column(String(250), nullable=False, name='producent')
    device_id = Column(String(250), nullable=False, name='id_urzadzenia')
    guid = Column(String(250), nullable=False, name='guid_urzadzenia')
    dev_type = Column(String(100), nullable=False, name='typ_urzadzenia')

    def __init__(self, _entity, id_user: int = None):
        super().__init__()
        self.id_user_keylogger = id_user
        if isinstance(_entity, USBInsertInfoEntity):
            self.date_time_info = datetime.strptime(_entity.date_time_info, Parameters.DB_KEYLOG_DATETIME_FORMAT)
            self.dev_recognize_as = _entity.dev_recognize_as
            self.insert_status = _entity.insert_status
            self.dev_name = _entity.dev_name
            self.description = _entity.description
            self.manufacturer = _entity.manufacturer
            self.device_id = _entity.device_id
            self.guid = _entity.guid
            self.dev_type = _entity.dev_type
        elif isinstance(_entity, dict):
            self.date_time_info = datetime.strptime(_entity['_date_time_info'], Parameters.DB_KEYLOG_DATETIME_FORMAT)
            self.dev_recognize_as = _entity['_dev_recognize_as']
            self.insert_status = _entity['_insert_status']
            self.dev_name = _entity['_dev_name']
            self.description = _entity['_description']
            self.manufacturer = _entity['_manufacturer']
            self.device_id = _entity['_device_id']
            self.guid = _entity['_guid']
            self.dev_type = _entity['_dev_type']
        self._logger = Logger.create_to_file_logger(logging_name='DeviceInfoEntity')

    def set_id_user_keylogger(self, id_user: int):
        self.id_user_keylogger = id_user

    def save_cached_device_in_db(self):
        self.create_new_session()
        self.save_entity(self)
        self.do_commit_session()
        self.close_session()
