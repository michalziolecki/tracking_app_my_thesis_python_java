from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer
from sqlalchemy.orm import relationship
from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from sqlalchemy import String, Integer, DateTime
from sqlalchemy import and_, or_, not_
from logIT.usb_module.file_operations.json_memory_info import RemoteMemoryInsertInfoEntity
from logIT.db_models_entities.device_entities.scan_remote_memory import ScanRemoteMemoryEntity
from logIT.develop_log.dev_log import Logger
from copy import copy
from datetime import datetime
from logIT.application_parameters.Parameters import Parameters
import threading

mutex = threading.Lock()


class RemoteMemoryInfoEntity(Base, AbstractEntityDAO):
    __tablename__ = 'informacje_dyski_zdalne'

    id = Column(Integer, primary_key=True, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False,
                               name='id_uzytkownicy_keylogger')
    date_time_info = Column(DateTime, nullable=False, name='data_operacji')  # timestamp in DB
    dev_recognize_as = Column(String(50), nullable=False, name='rozpoznany_jako')
    insert_status = Column(String(3), nullable=False, name='status_operacji')
    disk_name = Column(String(3), nullable=False, name='nazwa_dysku')
    description = Column(String(100), nullable=False, name='opis')
    file_system = Column(String(10), nullable=False, name='system_plikow')
    free_space = Column(Integer, nullable=False, name='wolna_przestrzen')
    total_space = Column(Integer, nullable=False, name='calkowita_przestrzen')
    volume_name = Column(String(250), nullable=False, name='nazwa_woluminu')
    volume_id = Column(String(250), nullable=False, name='identyfikator_woluminu')
    remote_disk_info_entity = relationship("ScanRemoteMemoryEntity")

    def __init__(self, _entity=None, id_user: int = None):
        super().__init__()
        self.disk_id_in_db = None
        self.id_user_keylogger = id_user
        if isinstance(_entity, RemoteMemoryInsertInfoEntity):
            self.date_time_info = datetime.strptime(_entity.date_time_info, Parameters.DB_KEYLOG_DATETIME_FORMAT)
            self.dev_recognize_as = _entity.dev_recognize_as
            self.insert_status = _entity.insert_status
            self.disk_name = _entity.disk_name
            self.description = _entity.description
            self.file_system = _entity.file_system
            self.free_space = round(int(_entity.free_space) / 1000)
            self.total_space = round(int(_entity.max_size) / 1000)
            self.volume_name = _entity.volume_name
            self.volume_id = _entity.volume_serial_number
        elif isinstance(_entity, dict):
            self.date_time_info = datetime.strptime(_entity['_date_time_info'], Parameters.DB_KEYLOG_DATETIME_FORMAT)
            self.dev_recognize_as = _entity['_dev_recognize_as']
            self.insert_status = _entity['_insert_status']
            self.disk_name = _entity['_disk_name']
            self.description = _entity['_description']
            self.file_system = _entity['_file_system']
            self.free_space = round(int(_entity['_free_space']) / 1000)
            self.total_space = round(int(_entity['_max_size']) / 1000)
            self.volume_name = _entity['_volume_name']
            self.volume_id = _entity['_volume_serial_number']
        self._logger = Logger.create_to_file_logger(logging_name='RemoteMemoryInfoEntity')

    def set_id_user_keylogger(self, id_user: int):
        self.id_user_keylogger = id_user

    def set_parameter_to_find_in_db(self, id_user: int, disk_name: str, date_time_info: datetime):
        self.id_user_keylogger = id_user
        self.disk_name = disk_name
        self.date_time_info = date_time_info

    def update_remote_memory_info_in_db(self):
        self.create_new_session()
        self.session.query(RemoteMemoryInfoEntity) \
            .filter(RemoteMemoryInfoEntity.id_user_keylogger == self.id_user_keylogger) \
            .filter(RemoteMemoryInfoEntity.disk_name == self.disk_name) \
            .filter(RemoteMemoryInfoEntity.date_time_info == self.date_time_info) \
            .update({'tracking_status': self.tracking_status,
                     'dev_recognize_as': self.dev_recognize_as,
                     'insert_status': self.insert_status,
                     'description': self.description,
                     'file_system': self.file_system,
                     'self.free_space': self.free_space,
                     'total_space': self.total_space,
                     'volume_name': self.volume_name,
                     'volume_id': self.volume_id
                     })

        self.do_commit_session()
        self.close_session()

    def save_remote_memory_info_in_db(self):
        self.create_new_session()
        self.save_entity(self)
        self.do_commit_session()
        self.close_session()

    def get_disk_id_from_db(self) -> int:
        try:
            mutex.acquire()
            self.create_new_session()
            disk_id_snapshot: list = self.session.query(RemoteMemoryInfoEntity) \
                .filter(RemoteMemoryInfoEntity.id_user_keylogger == self.id_user_keylogger) \
                .filter(RemoteMemoryInfoEntity.disk_name == self.disk_name) \
                .filter(RemoteMemoryInfoEntity.date_time_info == self.date_time_info).all()

            if len(disk_id_snapshot) == 0:
                self.disk_id_in_db = -1

            elif len(disk_id_snapshot) == 1:
                self.disk_id_in_db = copy(disk_id_snapshot[0].id)

            else:
                self._logger.warning(
                    msg='Duplicated remote disk, application use first but, fix this problem immediately!!! ')
                self.disk_id_in_db = copy(disk_id_snapshot[0].id)

            self.do_commit_session()
            self.close_session()
        finally:
            mutex.release()

        return self.disk_id_in_db
