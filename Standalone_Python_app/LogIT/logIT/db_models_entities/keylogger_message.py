from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, DateTime
from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from logIT.develop_log.dev_log import Logger


class KeyloggerMessage(Base, AbstractEntityDAO):

    __tablename__ = 'keylogger_wiadomosci'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False, name='id_uzytkownicy_keylogger')
    sentece = Column(String(1000), nullable=False, name='wiadomosc')
    date_time = Column(DateTime, nullable=False, name='kiedy_zebrano_wiadomosc')  # timestamp in DB

    def __init__(self, sentence, date_time):
        super().__init__()
        self.sentece = sentence
        self.date_time = date_time
        self._logger = Logger.create_to_file_logger(logging_name='KeyloggerMessage')

    def set_id_user_keylogger(self, id_user):
        self.id_user_keylogger = id_user

    def save_intercept_message_in_db(self):
        self.create_new_session()
        self.save_entity(self)
        self.do_commit_session()
        self.close_session()


