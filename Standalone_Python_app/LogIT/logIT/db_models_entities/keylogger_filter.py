from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer
from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from logIT.develop_log.dev_log import Logger
from copy import copy


class KeyloggerFilter(Base, AbstractEntityDAO):
    __tablename__ = 'keylogger_filtr'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False,
                               name='id_uzytkownicy_keylogger')
    filter_param = Column(String(50), nullable=True, name='filtr')

    def __init__(self, id_user: int):
        super().__init__()
        self.id_user_keylogger = id_user
        self._logger = Logger.create_to_file_logger(logging_name='KeyloggerFilter')

    def set_filter_str(self, filter_param):
        self.filter_param = filter_param

    def save_filter_in_db(self):
        self.create_new_session()
        self.save_entity(self)
        self.do_commit_session()
        self.close_session()

    def save_filter_list_in_db(self, filter_list):

        for filter_str in filter_list:
            entity = KeyloggerFilter(self.id_user_keylogger)
            entity.filter_param = filter_str
            entity.save_filter_in_db()

    def get_all_filters_for_user_from_db(self) -> list:
        list_of_filter = []
        self.create_new_session()
        filter_snapshot: list = self.session.query(KeyloggerFilter) \
            .filter(KeyloggerFilter.id_user_keylogger == self.id_user_keylogger).all()
        if filter_snapshot is not None and len(filter_snapshot) > 0:
            for filter_entity in filter_snapshot:
                filter_param = copy(filter_entity.filter_param)
                list_of_filter.append(filter_param)
        self.do_commit_session()
        self.close_session()

        return list_of_filter
