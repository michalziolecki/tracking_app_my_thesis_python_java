from sqlalchemy import Column
from sqlalchemy import String, Integer
from sqlalchemy.orm import relationship
from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from sqlalchemy import and_, or_, not_
from logIT.db_models_entities.keylogger_message import KeyloggerMessage
from logIT.db_models_entities.keylogger_filter import KeyloggerFilter
from logIT.db_models_entities.keylogger_settings import KeyloggerSettings
from logIT.db_models_entities.device_entities.device_info_entity import DeviceInfoEntity
from logIT.db_models_entities.device_entities.remote_disk_info_entity import RemoteMemoryInfoEntity
from logIT.db_models_entities.device_entities.untracked_partition import UntrackedPartitions
from logIT.develop_log.dev_log import Logger
from copy import copy
import threading

mutex = threading.Lock()


class UserKeylogger(Base, AbstractEntityDAO):
    __tablename__ = 'uzytkownicy_keylogger'

    id = Column(Integer, primary_key=True, name='id')
    host_name = Column(String(50), nullable=False, name='nazwa_komputera')
    user_name = Column(String(50), nullable=False, name='nazwa_uzytkownika_na_komputerze')
    description = Column(String(250), name='opis')
    messages = relationship("KeyloggerMessage")
    filters = relationship("KeyloggerFilter")
    keylog_settings = relationship("KeyloggerSettings")
    device_info_entity = relationship("DeviceInfoEntity")
    remote_disk_info_entity = relationship("RemoteMemoryInfoEntity")
    untracked_partitions = relationship("UntrackedPartitions")

    def __init__(self, host_name: str, user_name: str, description=''):
        super().__init__()
        self.id_in_db = None  # this field is a bucket to id from database, because after commit field id is empty
        self.host_name = host_name
        self.host_name_in_db = host_name
        self.user_name = user_name
        self.user_name_in_db = user_name
        self.description = description
        self._logger = Logger.create_to_file_logger(logging_name='KeyloggerWriter')

    def get_id_or_create_user_in_db(self) -> int:

        try:
            mutex.acquire()
            self.create_new_session()
            user_snapshot: list = self.session.query(UserKeylogger) \
                .filter(
                and_(UserKeylogger.user_name == self.user_name, UserKeylogger.host_name == self.host_name)).all()

            if len(user_snapshot) == 0:
                self.save_entity(self)
                new_user_snapshot: list = self.session.query(UserKeylogger) \
                    .filter(
                    and_(UserKeylogger.user_name == self.user_name, UserKeylogger.host_name == self.host_name)).all()

                if new_user_snapshot is not None and len(new_user_snapshot) > 0:
                    self.id_in_db = copy(new_user_snapshot[0].id)

            elif len(user_snapshot) == 1:
                self.id_in_db = user_snapshot[0].id

            else:
                self._logger.warning(
                    msg='Duplicated users, application use first but, fix this problem immediately!!! ')
                self.id_in_db = user_snapshot[0].id

            self.do_commit_session()
            self.close_session()

            if self.id_in_db is None:
                self._logger.error(msg='User does\'nt created in  DB by method: '
                                       + self.get_or_create_user_in_db.__name__ + ' method! ')
                raise NotImplementedError(
                    'User does\'nt created in  DB by method: ' + self.get_or_create_user_in_db.__name__ + ' method! ')
        finally:
            mutex.release()

        return self.id_in_db
