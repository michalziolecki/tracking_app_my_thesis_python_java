from logIT.usb_module.tracking.device_analyzer import DeviceAnalyzer
from logIT.usb_module.threads.usb_threads import *


class USBController:

    def __init__(self):
        self.remote_memory_tracker = RemoteMemoryTracker()
        self.device_analyzer = DeviceAnalyzer(self.remote_memory_tracker)
        self.device_tracker = DeviceTracker(self.device_analyzer, self.remote_memory_tracker)
        self._thread_event = USBThreadEvent(self.device_tracker)
        self._thread_period = USBThreadPeriod(self.device_tracker)
        self.remote_disk_tracker = RemoteMemoryTracker()
        self._thread_remote_disk = DiskDriversThread(self.remote_disk_tracker)

    def start_control(self):
        self._thread_event.start()
        self._thread_period.start()
        self._thread_remote_disk.start()

    def stop_control_gently(self):
        self._thread_period.stop_control_gently()
        self._thread_event.stop_control_gently()
        self._thread_remote_disk.stop_control_gently()

    def stop_control_urgent(self):
        self._thread_period.stop_control_gently()
        self._thread_event.stop_control_urgent()
        self._thread_remote_disk.stop_control_gently()
