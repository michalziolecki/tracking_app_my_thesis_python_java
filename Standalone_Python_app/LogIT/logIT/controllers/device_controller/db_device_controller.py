from logIT.controllers.device_controller.usb_controller import USBController
from logIT.usb_module.file_operations.usb_file_operations import USBDevicesWriter
from logIT.db_models_entities.user_keylogger import UserKeylogger
from logIT.db_models_entities.device_entities.blocking_usb import BlockUSB
from logIT.db_models_entities.device_entities.tracking_usb import TrackUSB
from logIT.db_models_entities.device_entities.admin_privileges import Privileges
from logIT.db_models_entities.device_entities.track_disk_drivers import TrackDiskDrivers
from logIT.db_models_entities.device_entities.device_info_entity import DeviceInfoEntity
from logIT.db_models_entities.device_entities.remote_disk_info_entity import RemoteMemoryInfoEntity
from logIT.db_models_entities.device_entities.scan_remote_memory import ScanRemoteMemoryEntity
from logIT.db_models_entities.device_entities.admin_privileges import Privileges
from logIT.db_models_entities.device_entities.untracked_partition import UntrackedPartitions
from logIT.usb_module.classes.scan_class import ScanFileEntity
from logIT.develop_log.dev_log import Logger
from logIT.application_parameters.Parameters import Parameters
from logIT.usb_module.threads.usb_threads import BlockPortThreadByExe
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.exc import OperationalError
from datetime import datetime
from threading import Thread
import getpass
import socket
import time
from copy import copy


class DatabaseDeviceController:

    def __init__(self, device_controller: USBController):
        self._device_controller = device_controller
        self.remote_memory_tracker = self._device_controller.remote_memory_tracker
        self.device_tracker = self._device_controller.device_tracker
        self.remote_disk_tracker = self._device_controller.remote_disk_tracker
        self._device_writer = USBDevicesWriter()
        self._working_status = True
        self._block_status = True
        self._remote_memory_tracking_status = True
        self._remote_memory_scan_status = True
        self._disk_drivers_tracking_status = True
        self._disk_drivers_scan_status = True
        self._connection_with_DB_status = True
        self._administrator_login = ""
        self._administrator_pass = ""
        self._actually_usr = UserKeylogger('', '')
        self.entity_dev_info_not_send_list = []
        self.entity_rem_mem_info_not_send_list = []
        self.entity_cached_files_from_rem_mem_not_send_list = []
        self._file_container_send_to_db = []
        self.remote_memory_tracker.file_container_send_to_db = self._file_container_send_to_db
        self.remote_disk_tracker.file_container_send_to_db = self._file_container_send_to_db
        self._logger = Logger.create_to_file_logger(logging_name='DatabaseDeviceController')

    def run_database_controller_with_device_controller(self):
        thread = Thread(target=self._start_control)
        thread.start()

    def end_database_controller_with_device_controller(self):
        self._working_status = False
        try:
            self._device_controller.stop_control_gently()
        except:
            self._logger.warning('Warning in method ' + self.end_database_controller_with_device_controller.__name__
                                 + '. Application try to stop, stopped thread: Device Controller was stopped.')

    def _start_control(self):
        self._device_controller.start_control()
        self._working_status = True
        while self._working_status:
            self._control_select_operations()
            if self._connection_with_DB_status:
                self._control_insert_operations()
            if not self._connection_with_DB_status:
                self._write_not_send_files_to_file()
                self._run_exe_to_block_port()
            time.sleep(Parameters.DB_DEVICE_PERIODIC_CONNECTION_TIME)

    def _write_not_send_files_to_file(self):
        if len(self._file_container_send_to_db) > 0:
            files_info_to_write = copy(self._file_container_send_to_db)
            self._file_container_send_to_db.clear()
            write_status = self._device_writer.write_usb_devices_insert_info(files_info_to_write,
                                                                                    Parameters.REMOTE_MEMORY_FILES_FILE_NAME)
            if write_status == 1:
                self._file_container_send_to_db.extend(files_info_to_write)

    def _control_select_operations(self):
        try:
            self._check_actually_user()
            self._block_status = self._check_block_status()
            self._remote_memory_tracking_status, self._remote_memory_scan_status = self._check_remote_memory_statuses()
            self._disk_drivers_tracking_status, self._disk_drivers_scan_status = self._check_disk_drivers_statuses()
            self.remote_memory_tracker.untracked_disk_partitions = self._check_untracked_disk_partitions()
            # self._administrator_login, self._administrator_pass = self._check_privileges()
            self._connection_with_DB_status = True
            self.remote_memory_tracker.connection_with_DB_status = True
            self.remote_disk_tracker.connection_with_DB_status = True
        except OperationalError as op:
            self._logger.warning("Connection with Database don't exist, stacktrace: " + str(op.args))
            self.device_tracker._current_block_usb_status = True
            self._remote_memory_tracking_status = True
            self._remote_memory_scan_status = True
            self._disk_drivers_tracking_status = True
            self._disk_drivers_scan_status = True
            self._connection_with_DB_status = False
            self.remote_memory_tracker.connection_with_DB_status = False
            self.remote_disk_tracker.connection_with_DB_status = False
            self.remote_disk_tracker.untracked_disk_partitions = Parameters.UNTRACKED_PARTITIONS_IN_OFFLINE
        except:
            self._logger.warning("Unknow erorr in def _control_select_operations(self):")

        if self._connection_with_DB_status:
            self._set_param_in_device_threads()
            self._run_exe_to_block_port()

    def _check_block_status(self) -> bool:
        block_usb = BlockUSB(self._actually_usr.id_in_db)
        return block_usb.get_block_port_status_from_db()

    def _check_remote_memory_statuses(self) -> tuple:
        track_usb = TrackUSB(self._actually_usr.id_in_db)
        return track_usb.get_tracking_and_scan_status_from_db()

    def _check_disk_drivers_statuses(self) -> tuple:
        track_drivers = TrackDiskDrivers(self._actually_usr.id_in_db)
        return track_drivers.get_tracking_and_scan_status_from_db()

    def _check_actually_user(self):
        user_name = getpass.getuser()
        host_name = socket.gethostname()
        if self._actually_usr.user_name_in_db != user_name or self._actually_usr.host_name_in_db != host_name or \
                self._actually_usr.id_in_db is None:
            self._actually_usr = UserKeylogger(host_name, user_name)
            self._actually_usr.get_id_or_create_user_in_db()

    def _check_admin_pass(self):
        admin_privileges = Privileges(self._actually_usr.id_in_db)
        self._administrator_login, self._administrator_pass = admin_privileges.get_login_and_password_from_db()

    def _set_param_in_device_threads(self):
        # actualize statuses usb devices
        self.device_tracker._current_block_usb_status = self._block_status
        self.device_tracker._track_usb_status_by_event = self._remote_memory_tracking_status
        self.device_tracker._track_usb_status_by_period_time = self._remote_memory_tracking_status
        self.remote_memory_tracker._track_usb_remote_memory_status = self._remote_memory_tracking_status
        self.remote_memory_tracker._scan_content_of_disk_status = self._remote_memory_scan_status
        # actualize statuses disk drivers
        self.remote_disk_tracker._track_remote_disk_drivers_status = self._disk_drivers_tracking_status
        self.remote_disk_tracker._scan_content_of_disk_status = self._disk_drivers_scan_status

    def _run_exe_to_block_port(self):
        block_ports = BlockPortThreadByExe(self.device_tracker)
        block_ports.start()
        block_ports.join(timeout=4)
        if block_ports.isAlive():
            block_ports.stop()

    def _check_untracked_disk_partitions(self):
        user_name = getpass.getuser()
        host_name = socket.gethostname()
        id_usr = self._check_id_usr(host_name, user_name)
        untracked_disk = UntrackedPartitions(id_usr)
        untracked_list = []
        untracked_list = untracked_disk.get_all_partition_disks_for_user_from_db()

        return untracked_list

    def _check_privileges(self) -> tuple:
        if self._actually_usr.id_in_db is None:
            self._check_actually_user()
        priviliges = Privileges(self._actually_usr.id_in_db)
        return priviliges.get_login_and_password_from_db()

    def _control_insert_operations(self):
        self._send_device_info_entity_to_db()
        self._send_remote_disk_info_entity_to_db()
        self._send_to_db_cached_files_in_remote_memory_from_file()
        self._send_to_db_cached_files_in_remote_memory_from_ram()

    def _send_device_info_entity_to_db(self):
        path_to_file = Parameters.KEYLOG_FILE_PATH + '/' + Parameters.USB_DEVICES_INFO_FILE_NAME
        device_json_list_to_send, response = self._device_writer.wait_and_read_file(path_to_file)
        if response == 0:
            for device in device_json_list_to_send:
                if self._actually_usr.user_name_in_db != device['_nick_name'] or \
                        self._actually_usr.host_name_in_db != device['_host_name']:
                    user = UserKeylogger(device['_host_name'], device['_nick_name'])
                    try:
                        user.get_id_or_create_user_in_db()
                    except SQLAlchemyError as exception:
                        self._connection_with_DB_status = False
                        self.remote_memory_tracker.connection_with_DB_status = False
                        self.remote_disk_tracker.connection_with_DB_status = False
                        self._logger.warning("SQLAlchemyError, args: %s", str(exception.args))
                    except:
                        self._logger.warning("Unknow erorr in def _send_device_info_entity_to_db(self):")

                    self._save_device_info_entity_in_db(device, user.id_in_db)
                elif self._actually_usr.id_in_db is not None:
                    self._save_device_info_entity_in_db(device, self._actually_usr.id_in_db)
                else:
                    self._check_actually_user()
                    self._save_device_info_entity_in_db(device, self._actually_usr.id_in_db)

        if len(self.entity_dev_info_not_send_list) > 0:
            status = self._device_writer.write_json_to_file(self.entity_dev_info_not_send_list,
                                                            Parameters.USB_DEVICES_INFO_FILE_NAME)
            if status == 0:
                self.entity_dev_info_not_send_list.clear()

    def _save_device_info_entity_in_db(self, device: dict, usr_id: int):
        try:
            device_info_entity = DeviceInfoEntity(device, usr_id)
            device_info_entity.save_cached_device_in_db()
        except SQLAlchemyError as exception:
            self._connection_with_DB_status = False
            self.remote_memory_tracker.connection_with_DB_status = False
            self.remote_disk_tracker.connection_with_DB_status = False
            self.entity_dev_info_not_send_list.append(device)
            self._logger.warning("SQLAlchemyError, args: %s", str(exception.args))
        except:
            self._logger.warning("Unknow erorr in def _save_device_info_entity_in_db")

    def _send_remote_disk_info_entity_to_db(self):
        path_to_file = Parameters.KEYLOG_FILE_PATH + '/' + Parameters.REMOTE_MEMORY_INFO_FILE_NAME
        device_json_list_to_send, response = self._device_writer.wait_and_read_file(path_to_file)
        if response == 0:
            self._check_double_entities(device_json_list_to_send)
            for device in device_json_list_to_send:
                if self._actually_usr.user_name_in_db != device['_nick_name'] or \
                        self._actually_usr.host_name_in_db != device['_host_name']:
                    user = UserKeylogger(device['_host_name'], device['_nick_name'])
                    try:
                        user.get_id_or_create_user_in_db()
                    except SQLAlchemyError as exception:
                        self._connection_with_DB_status = False
                        self.remote_memory_tracker.connection_with_DB_status = False
                        self.remote_disk_tracker.connection_with_DB_status = False
                        self._logger.warning("SQLAlchemyError, args: %s", str(exception.args))

                    self._save_remote_memory_info_entity_in_db(device, user.id_in_db)
                elif self._actually_usr.id_in_db is not None:
                    self._save_remote_memory_info_entity_in_db(device, self._actually_usr.id_in_db)
                else:
                    self._check_actually_user()
                    self._save_remote_memory_info_entity_in_db(device, self._actually_usr.id_in_db)

        if len(self.entity_rem_mem_info_not_send_list) > 0:
            status = self._device_writer.write_json_to_file(self.entity_rem_mem_info_not_send_list,
                                                            Parameters.REMOTE_MEMORY_INFO_FILE_NAME)
            if status == 0:
                self.entity_rem_mem_info_not_send_list.clear()

    def _check_double_entities(self, device_json_list_to_send: list):
        tmp_list = copy(device_json_list_to_send)
        rm_index = -1

        for tmp_dev in tmp_list:
            if tmp_list.count(tmp_dev) > 1:
                for index, device in enumerate(device_json_list_to_send, start=0):
                    if device.date_time_info == tmp_dev.date_time_info and \
                            device.scanned_disk == tmp_dev.scanned_disk:
                        rm_index = index
                        break
                if rm_index != -1:
                    device_json_list_to_send.pop(rm_index)
                    rm_index = -1

    def _save_remote_memory_info_entity_in_db(self, device: dict, usr_id: int):
        try:
            remote_memory_info_entity = RemoteMemoryInfoEntity(device, usr_id)
            remote_memory_info_entity.save_remote_memory_info_in_db()
        except SQLAlchemyError as exception:
            self._connection_with_DB_status = False
            self.remote_memory_tracker.connection_with_DB_status = False
            self.remote_disk_tracker.connection_with_DB_status = False
            self.entity_rem_mem_info_not_send_list.append(device)
            self._logger.warning("SQLAlchemyError, args: %s", str(exception.args))
        except:
            self._logger.warning("Unknow erorr in def _save_remote_memory_info_entity_in_db")

    def _send_to_db_cached_files_in_remote_memory_from_file(self):
        path_to_file = Parameters.KEYLOG_FILE_PATH + '/' + Parameters.REMOTE_MEMORY_FILES_FILE_NAME
        device_json_list_to_send, response = self._device_writer.wait_and_read_file(path_to_file)
        id_usr = -1
        id_disk = -1
        if response == 0:
            for device in device_json_list_to_send:
                id_usr = self._check_id_usr_from_json(device)
                if id_usr != -1 and id_usr is not None:
                    disk_name = device['_scanned_disk']
                    date_time_info = device['_date_time_info']
                    id_disk = self._check_id_disk_from_json(id_usr, disk_name, date_time_info)
                    if id_disk != -1 and id_disk is not None:
                        self._save_cached_files_from_remote_memory_entity_in_db(device, id_usr, id_disk)
                    else:
                        self.entity_cached_files_from_rem_mem_not_send_list.append(device)
                else:
                    self.entity_cached_files_from_rem_mem_not_send_list.append(device)

        if len(self.entity_cached_files_from_rem_mem_not_send_list) > 0:
            status = self._device_writer.write_json_to_file(self.entity_cached_files_from_rem_mem_not_send_list,
                                                            Parameters.REMOTE_MEMORY_FILES_FILE_NAME)
            if status == 0:
                self.entity_cached_files_from_rem_mem_not_send_list.clear()

    def _send_to_db_cached_files_in_remote_memory_from_ram(self):
        id_usr = -1
        id_disk = -1
        tmp_file_container_send_to_db = copy(self._file_container_send_to_db)
        self._file_container_send_to_db.clear()
        for device in tmp_file_container_send_to_db:
            device: ScanFileEntity
            id_usr = self._check_id_usr(device.host_name, device.nick_name)
            if id_usr != -1 and id_usr is not None:
                disk_name = device.scanned_disk
                date_time_info = datetime.strptime(device.date_time_info, Parameters.DB_KEYLOG_DATETIME_FORMAT)
                id_disk = self._check_id_disk_from_json(id_usr, disk_name, date_time_info)
                if id_disk != -1 and id_disk is not None:
                    self._save_cached_files_from_remote_memory_entity_in_db(device, id_usr, id_disk)
                else:
                    self._file_container_send_to_db.append(device)
            else:
                self._file_container_send_to_db.append(device)

    def _check_id_usr_from_json(self, device: dict) -> int:
        id_usr = -1
        if self._actually_usr.user_name_in_db != device['_nick_name'] or \
                self._actually_usr.host_name_in_db != device['_host_name']:
            user = UserKeylogger(device['_host_name'], device['_nick_name'])
            try:
                user.get_id_or_create_user_in_db()
            except SQLAlchemyError as exception:
                self._connection_with_DB_status = False
                self.remote_memory_tracker.connection_with_DB_status = False
                self.remote_disk_tracker.connection_with_DB_status = False
                self._logger.warning("SQLAlchemyError, args: %s", str(exception.args))
            except:
                self._logger.warning("Unknown erro in  def _check_id_usr_from_json")
            id_usr = user.id_in_db
        elif self._actually_usr.id_in_db is not None:
            id_usr = self._actually_usr.id_in_db
        else:
            self._check_actually_user()
            id_usr = self._actually_usr.id_in_db

        return id_usr

    def _check_id_usr(self, host_usr: str, nick: str) -> int:
        id_usr = -1
        if self._actually_usr.user_name_in_db != nick or \
                self._actually_usr.host_name_in_db != host_usr:
            user = UserKeylogger(host_usr, nick)
            try:
                user.get_id_or_create_user_in_db()
            except SQLAlchemyError as exception:
                self._connection_with_DB_status = False
                self.remote_memory_tracker.connection_with_DB_status = False
                self.remote_disk_tracker.connection_with_DB_status = False
                self._logger.warning("SQLAlchemyError, args: %s", str(exception.args))
            except:
                self._logger.warning("Unknown erro in  def _check_id_usr")

            id_usr = user.id_in_db
        elif self._actually_usr.id_in_db is not None:
            id_usr = self._actually_usr.id_in_db
        else:
            self._check_actually_user()
            id_usr = self._actually_usr.id_in_db

        return id_usr

    def _check_id_disk_from_json(self, id_user: int, disk_name: str, date_time_info: datetime) -> int:
        remote_memory_info_entity = RemoteMemoryInfoEntity()
        remote_memory_info_entity.set_parameter_to_find_in_db(id_user, disk_name, date_time_info)
        id_disk = -1
        try:
            id_disk = remote_memory_info_entity.get_disk_id_from_db()
        except SQLAlchemyError as exception:
            self._connection_with_DB_status = False
            self.remote_memory_tracker.connection_with_DB_status = False
            self.remote_disk_tracker.connection_with_DB_status = False
            self._logger.warning("SQLAlchemyError, args: %s", str(exception.args))
        except:
            self._logger.warning("Unknow erorr in  def _check_id_disk_from_json")
        return id_disk

    # device is dict or ScanFileEntity
    def _save_cached_files_from_remote_memory_entity_in_db(self, device, usr_id: int, disk_id: int):
        try:
            remote_memory_info_entity = ScanRemoteMemoryEntity(device, usr_id, disk_id)
            remote_memory_info_entity.save_remote_memory_info_in_db()
        except SQLAlchemyError as exception:
            if isinstance(device, ScanFileEntity):
                self._file_container_send_to_db.append(device)
            if isinstance(device, dict):
                self.entity_cached_files_from_rem_mem_not_send_list.append(device)
            self._connection_with_DB_status = False
            self.remote_memory_tracker.connection_with_DB_status = False
            self.remote_disk_tracker.connection_with_DB_status = False
            self._logger.warning("SQLAlchemyError, args: %s", str(exception.args))
        except:
            self._logger.warning("Unknown error in  def _save_cached_files_from_remote_memory_entity_in_db")
