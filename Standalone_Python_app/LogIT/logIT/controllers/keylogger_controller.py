from logIT.keylogger.sentence_analyzer import SentenceAnalyzer
from logIT.keylogger.key_listener import KeyListener
from logIT.keylogger.click_listener import ClickListener


class KeyloggerController:

    def __init__(self):
        self.analyzer = SentenceAnalyzer()
        self.watcher = KeyListener(analyzer_obj=self.analyzer)
        self.clicker = ClickListener(analyzer_obj=self.analyzer)

    def start_listening(self):
        self.watcher.start_listening()
        self.clicker.start_listening()

    def start_listening_watcher(self):
        self.watcher.start_listening()

    def stop_listening_watcher(self):
        self.watcher.stop_listening()

    def stop_listening(self):
        self.watcher.stop_listening()
        self.clicker.stop_listening()
