from enum import Enum


class FilterObject(Enum):
    mouse_pl = 'mysz'
    mouse_eng = 'mouse'
    pendrive_pl = 'pamięci masowej usb'
    pendrive_eng = 'memory usb'
    phone_eng = 'phone'
    phone_pl = 'telefon'
    mass_usb_memory_eng = 'mass storage'
    mass_usb_memory_pl = 'pamięć masowa'
    camera_eng = 'camera'
    camera_pl = 'kamera'
    printer_pl = 'drukarka'
    printer_eng = 'printer'
    keyboard_eng = 'keyboard'
    keyboard_pl = 'klawiatury'
    card_eng = 'card'
    card_pl = 'kart'
    headphone_eng = 'słuchawki'
    headphone_pl = 'headphone'

