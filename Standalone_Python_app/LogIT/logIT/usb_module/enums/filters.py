from logIT.usb_module.enums.enum_excluded_obj import ExcludedObject
from logIT.usb_module.enums.enum_filter_object import FilterObject


def get_excluded_filters():
    filter_list = []
    for item in ExcludedObject:
        filter_list.append(item.value)
    return filter_list


def get_known_filters():
    filter_list = []
    for item in FilterObject:
        filter_list.append(item.value)
    return filter_list
