# outer blocking
import os
from io import TextIOWrapper
import json
from json.decoder import JSONDecodeError
import portalocker
import time
# inner blocking
from logIT.develop_log.dev_log import Logger
from logIT.usb_module.file_operations.usb_json_entity import USBInsertInfoEntity
from logIT.application_parameters.Parameters import Parameters


class USBDevicesWriter:

    def __init__(self, directory=Parameters.KEYLOG_FILE_PATH):
        self._directory = directory
        self._logger = Logger.create_to_file_logger(logging_name='USBDevicesWriter')

    @property
    def directory(self) -> str:
        return self._directory

    def _create_directory_path(self):
        if not os.path.isdir(self._directory):
            os.makedirs(self._directory)
            self._logger.info(msg="Directory for usb json created.")

    # return list of json
    def read_usb_json(self, file_path) -> tuple:
        json_usb_entities = []
        status = 1
        if os.path.isfile(file_path):
            file_handler = open(file_path, 'r+')
            try:
                portalocker.lock(file_handler, portalocker.LOCK_EX | portalocker.LOCK_NB)
                json_usb_entities = json.load(file_handler)
                status = 0
            except portalocker.LockException as le:
                self._logger.warning('Exception with locking file in method' + self.read_usb_json.__name__ +
                                     ', stacktrace: ' + str(le))
                file_handler.close()
                return json_usb_entities, status
            except JSONDecodeError as json_err:
                #  self._logger.warning('Existing file was empty, stacktrace: ' + str(json_err.args))
                file_handler.close()
                json_usb_entities.clear()

            # Upewnic sie co do trunc
            if status == 0:
                file_handler.truncate(0)
                file_handler.write('[]')
            file_handler.close()
        return json_usb_entities, status

    def wait_and_read_file(self, file_path) -> tuple:
        read_response = 1
        iterator = 0
        json_usb_entities = []
        while read_response == 1 and iterator < 5:
            json_usb_entities, read_response = self.read_usb_json(file_path)
            time.sleep(0.1)
            iterator += 1
        return json_usb_entities, read_response

    def clear_existing_file(self, file_path) -> int:
        self._create_directory_path()

        status = 1
        try_number = 1
        while status == 1 and try_number <= 5:
            file_handler = open(file_path, 'w')
            try:
                portalocker.lock(file_handler, portalocker.LOCK_EX | portalocker.LOCK_NB)
                file_handler.write('[]')
                status = 0
            except portalocker.LockException as le:
                self._logger.warning('Exception with locking file in method'
                                     + self.clear_existing_file.__name__ +
                                     ', stacktrace: ' + str(le))
                file_handler.close()
                status = 1

            file_handler.close()
            time.sleep(0.1)
            try_number += 1

        return status

    # param: list of USBInsertInfoEntity
    def write_usb_devices_insert_info(self, usb_insert_info_entity_list: list,
                                      file_name=Parameters.USB_DEVICES_INFO_FILE_NAME) -> int:
        path_to_file = self._directory + '/' + file_name

        json_usb_info_exist_entities, response = self.wait_and_read_file(path_to_file)

        usb_entity: USBInsertInfoEntity
        # indent=4 set tabulator line by line (better readable),
        # in future this param will has droppped before beta version
        json_usb_entities: str = json.dumps(
            obj=[usb_entity.get_full_dict_of_entity() for usb_entity in usb_insert_info_entity_list],
            separators=(',', ': '))
        json_usb_entities: list = json.loads(json_usb_entities)

        for json_entity in json_usb_entities:
            json_usb_info_exist_entities.append(json_entity)

        all_json_usb_info_exist_entities: str = json.dumps(obj=json_usb_info_exist_entities, separators=(',', ': '))

        self._create_directory_path()
        status = 1
        try_number = 1
        while status == 1 and try_number <= 5:
            file_handler = open(path_to_file, 'w')
            try:
                portalocker.lock(file_handler, portalocker.LOCK_EX | portalocker.LOCK_NB)
                file_handler.write(all_json_usb_info_exist_entities)
                status = 0
            except portalocker.LockException as le:
                self._logger.warning(
                    'Exception with locking file in method' + self.write_usb_devices_insert_info.__name__ +
                    ', stacktrace: ' + str(le))
                status = 1
                file_handler.close()
            time.sleep(0.1)
            try_number += 1
            file_handler.close()

        return status

    """
        Method to write object in json format to file
        :param list with json's and string
        :return integer status
    """
    def write_json_to_file(self, json_list: list, file_name: str) -> int:

        str_json_entities: str = json.dumps(obj=json_list, separators=(',', ': '))

        path_to_file: str = self._directory + '/' + file_name
        self._create_directory_path()
        status = 1
        try_number = 1
        while status == 1 and try_number <= 5:
            file_handler: TextIOWrapper = open(path_to_file, 'w')
            try:
                portalocker.lock(file_handler, portalocker.LOCK_EX | portalocker.LOCK_NB)
                file_handler.write(str_json_entities)
                status = 0
            except portalocker.LockException as le:
                self._logger.warning(
                    'Exception with locking file in method'
                    + self.write_usb_devices_insert_info.__name__ +
                    ', stacktrace: ' + str(le))
                status = 1
                file_handler.close()

            time.sleep(0.1)
            try_number += 1
            file_handler.close()

        return status
