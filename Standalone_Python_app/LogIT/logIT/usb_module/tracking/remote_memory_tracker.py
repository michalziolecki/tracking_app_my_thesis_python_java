from logIT.application_parameters.Parameters import Parameters
from logIT.usb_module.file_operations.usb_file_operations import USBDevicesWriter
from logIT.usb_module.classes.memory_info_classes import *
from logIT.usb_module.file_operations.json_memory_info import RemoteMemoryInsertInfoEntity
from logIT.usb_module.tracking.memory_scaner import MemoryScanner
from logIT.develop_log.dev_log import Logger
import wmi
import pythoncom
import time
import getpass
from datetime import datetime
import socket
import copy
import _thread


class RemoteMemoryTracker:

    def __init__(self):
        self._remote_memory_writer = USBDevicesWriter()
        self._track_remote_disk_drivers_status = True
        self._track_usb_remote_memory_status = True
        self._scan_content_of_disk_status = True
        self._stop_thread_tracking_disk_drivers = False
        self._stop_thread_tracking_usb_memory = False
        self.connection_with_DB_status = True
        self._time_control = Parameters.DISK_DRIVERS_SCAN_PERIODIC_TIME
        self._existed_remote_memory_container = set()
        self.cached_files_in_memory_to_write = []
        self.file_container_send_to_db = []
        self.remote_memory_container_to_write = []
        self.scan_thread_counter = set()
        self.untracked_disk_partitions = Parameters.UNTRACKED_PARTITIONS_IN_OFFLINE
        self._logger = Logger.create_to_file_logger(logging_name='Remote_Memory_Tracker')

    @property
    def track_remote_disk_status(self) -> bool:
        return self._track_remote_disk_drivers_status

    @property
    def scan_content_of_disk_status(self) -> bool:
        return self._scan_content_of_disk_status

    def set_track_remote_disk_status(self, status: bool):
        self._track_remote_disk_drivers_status = status

    def set_scan_content_of_disk_status(self, status):
        self._scan_content_of_disk_status = status

    def stop_thread_tracking_disk(self, status):
        self._stop_thread_tracking_disk_drivers = status

    def track_disk_drivers_information(self):
        pythoncom.CoInitialize()
        while not self._stop_thread_tracking_disk_drivers:

            if self._track_remote_disk_drivers_status and self._time_control <= 0:
                finger_print = {
                    'nick_name': getpass.getuser(),
                    'host_name': socket.gethostname(),
                    'date_time_info': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                }
                self.control_memory_devices(finger_print, self._check_drivers)
                self.write_cached_remote_memory_to_file()
                self.write_cached_files_to_file()
                self._time_control = Parameters.DISK_DRIVERS_SCAN_PERIODIC_TIME
            elif not self._track_remote_disk_drivers_status and self._time_control <= 0:
                self._time_control = Parameters.DISK_DRIVERS_SCAN_PERIODIC_TIME

            time.sleep(0.5)
            self._time_control -= 0.5
        pythoncom.CoUninitialize()

    def track_usb_remote_memory(self, finger_print: dict, added_status=True):
        pythoncom.CoInitialize()
        if self._track_usb_remote_memory_status:
            self.control_memory_devices(finger_print, self._check_usb_remote_memory, added_status)
            self.write_cached_remote_memory_to_file()
        pythoncom.CoUninitialize()

    def control_memory_devices(self, finger_print: dict, reference_to_check_function=None, added_status=True):
        status = 1
        try_number = 1
        new_cached_remote_memory_set = set()
        while status == 1 and try_number <= 5:
            try:
                remote_memory = reference_to_check_function()
                new_cached_remote_memory_set.update(remote_memory)
                status = 0
            except wmi.x_wmi as e:
                self._logger.info("Remote memory suddenly took out when wmi parsing object, info: %s", e.info)
                status = 1
                new_cached_remote_memory_set.clear()
            except Exception as e:
                self._logger.info("Something went wrong when wmi check remote memory, info: %s", e.args)
                status = 1
                new_cached_remote_memory_set.clear()
            try_number += 1

        if status == 0:
            added_device_set = new_cached_remote_memory_set.difference(self._existed_remote_memory_container)
            removed_device_set = self._existed_remote_memory_container.difference(new_cached_remote_memory_set)
            self._add_remote_memory_to_container(added_device_set, InsertStatus.set_in, finger_print)
            self._add_remote_memory_to_container(removed_device_set, InsertStatus.set_out, finger_print)
            self._existed_remote_memory_container = new_cached_remote_memory_set
            if self.scan_content_of_disk_status and added_status:
                self._run_memory_scanner(added_device_set, finger_print)

    def _run_memory_scanner(self, cached_devices: set, finger_print: dict):
        for device in cached_devices:
            device: AbstractRemoteMemoryInfo
            self._logger.debug("Start skanowania dla dysku: " + device.disk_name)
            scanner = MemoryScanner(device.disk_name, finger_print, self.cached_files_in_memory_to_write,
                                    self.file_container_send_to_db, self.connection_with_DB_status)
            try:
                param_tuple = (self.scan_thread_counter,)
                _thread.start_new_thread(scanner.scan_memory, param_tuple)
            except Exception as e:
                self._logger.error("Can't run scanner in alone thread, exception args: ", e.args)

    def write_cached_files_to_file(self):
        if len(self.cached_files_in_memory_to_write) > 0 and len(self.scan_thread_counter) > 0:
            files_info_to_write = copy.copy(self.cached_files_in_memory_to_write)
            self.cached_files_in_memory_to_write.clear()
            write_status = self._remote_memory_writer.write_usb_devices_insert_info(files_info_to_write,
                                                                                    Parameters.REMOTE_MEMORY_FILES_FILE_NAME)
            if write_status == 1:
                self.cached_files_in_memory_to_write.extend(files_info_to_write)

    def _add_remote_memory_to_container(self, memory_set: set, insert_status: InsertStatus, finger_print: dict):

        for device in memory_set:
            device: AbstractRemoteMemoryInfo
            dev_to_write = copy.copy(device)
            dev_to_write._insert_status = insert_status.value
            new_device = RemoteMemoryInsertInfoEntity(dev_to_write, finger_print)
            self.remote_memory_container_to_write.append(new_device)

    def write_cached_remote_memory_to_file(self):
        if len(self.remote_memory_container_to_write) > 0:
            remote_memory_insert_to_writes = copy.copy(self.remote_memory_container_to_write)
            self.remote_memory_container_to_write.clear()
            write_status = self._remote_memory_writer.write_usb_devices_insert_info(remote_memory_insert_to_writes,
                                                                                    Parameters.REMOTE_MEMORY_INFO_FILE_NAME)
            if write_status == 1:
                self.remote_memory_container_to_write.extend(remote_memory_insert_to_writes)

    def _check_drivers(self) -> set:
        drive_set = set()
        drive_list = self._list_cdrom_disks()
        for drive in drive_list:

            if drive.Access:
                cached_disk = RemoteCDMemoryInf(drive)
                drive_set.add(cached_disk)

        return drive_set

    def _check_usb_remote_memory(self) -> set:
        remote_memory_set = set()

        try_find_disk = 0
        while try_find_disk < 4:
            #  Add pendrive
            memory_list = self._list_remote_disks()
            for remote_mem in memory_list:
                cached_memory = RemoteDiskMemoryInf(remote_mem)
                remote_memory_set.add(cached_memory)

            #  Add mass memory disk
            memory_local_disk = self._list_local_disks()
            for remote_mem in memory_local_disk:
                if remote_mem.Name not in self.untracked_disk_partitions:
                    cached_memory = RemoteDiskMemoryInf(remote_mem)
                    remote_memory_set.add(cached_memory)

            try_find_disk += 1
            time.sleep(0.5)  # WAIT FOR INSTALL DISK TIME OPERATION
            if len(remote_memory_set) > 0 and try_find_disk > 1:
                break

        return remote_memory_set

    def _list_local_disks(self) -> list:
        local_disk_list = []
        windows_method_container = wmi.WMI()
        for disk in windows_method_container.Win32_LogicalDisk(DriveType=3):
            local_disk_list.append(disk)

        return local_disk_list

    def _list_remote_disks(self) -> list:
        remote_disk_list = []
        windows_method_container = wmi.WMI()
        for disk in windows_method_container.Win32_LogicalDisk(DriveType=2):
            remote_disk_list.append(disk)

        return remote_disk_list

    def _list_cdrom_disks(self) -> list:
        cdrom_disks_list = []
        windows_method_container = wmi.WMI()
        for disk in windows_method_container.Win32_LogicalDisk(DriveType=5):
            cdrom_disks_list.append(disk)

        return cdrom_disks_list

    def _list_all_disks_devices(self):
        disk_devices_list = []
        windows_method_container = wmi.WMI()
        wql = "Select * From Win32_LogicalDisk"
        for item in windows_method_container.query(wql):
            disk_devices_list.append(item)

        return disk_devices_list
