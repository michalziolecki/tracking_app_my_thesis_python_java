from logIT.develop_log.dev_log import Logger
from logIT.usb_module.tracking.device_analyzer import DeviceAnalyzer
from logIT.usb_module.file_operations.usb_file_operations import USBDevicesWriter
from logIT.usb_module.tracking.remote_memory_tracker import RemoteMemoryTracker
from logIT.application_parameters.Parameters import Parameters
import wmi
import pythoncom
import subprocess
import os
from copy import copy
import time


class DeviceTracker:

    def __init__(self, device_analyzer: DeviceAnalyzer, remote_memory_tracker: RemoteMemoryTracker):
        self._device_analyzer = device_analyzer
        self._remote_memory_tracker = remote_memory_tracker
        self._time_control = Parameters.USB_DEVICES_PERIODIC_TIME
        self._device_writer = USBDevicesWriter()
        self._process_watcher_stop = False
        self._track_usb_status_by_event = True
        self._track_usb_status_by_period_time = True
        self._current_block_usb_status = True
        self._previous_block_usb_status = False
        self._app_start = True
        self._logger = Logger.create_to_file_logger(logging_name='USB tracker')

    @property
    def device_analyzer(self) -> DeviceAnalyzer:
        return self._device_analyzer

    @property
    def time_control(self) -> int:
        return self._time_control

    @property
    def process_watcher_stop(self) -> bool:
        return self._process_watcher_stop

    @property
    def track_usb_status_by_event(self) -> bool:
        return self._track_usb_status_by_event

    @property
    def track_usb_status_by_period_time(self) -> bool:
        return self._track_usb_status_by_period_time

    def set_process_watcher_stop(self, process_watcher_flag: bool):
        self._process_watcher_stop = process_watcher_flag

    def set_track_usb_status_by_event(self, track_usb_flag: bool):
        self._track_usb_status_by_event = track_usb_flag

    def set_track_usb_status_by_period_time(self, track_usb_flag: bool):
        self._track_usb_status_by_period_time = track_usb_flag

    def track_usb_by_events(self):

        while not self._process_watcher_stop:
            try:
                pythoncom.CoInitialize()
                windows_method_container = wmi.WMI()
                # actions: “creation”, “deletion”, “modification” or “operation”
                process_watcher = windows_method_container.Win32_Process.watch_for("creation")
                new_process: wmi._wmi_event = process_watcher()
            except wmi.x_wmi as e:
                self._logger.info("Windows event method throws wmi exception, info: %s", e.info)
                pythoncom.CoUninitialize()
            except Exception as e:
                self._logger.info("Something when wrong in windows event method, info: %s", e.args)
                pythoncom.CoUninitialize()

            if self._track_usb_status_by_event:
                self.transport_usb_insert_entites_to_file()
                self._time_control = Parameters.USB_DEVICES_PERIODIC_TIME

        pythoncom.CoUninitialize()

    def track_usb_by_period_of_time(self):
        pythoncom.CoInitialize()
        while not self._process_watcher_stop:
            if self._track_usb_status_by_period_time and self._time_control <= 0:
                self.transport_usb_insert_entites_to_file(period_scan=True)
                self._time_control = Parameters.USB_DEVICES_PERIODIC_TIME
            # elif not self._track_usb_status_by_period_time and self.time_control <= 0:
            #     self._time_control = USB_DEVICES_PERIODIC_TIME

            time.sleep(0.5)
            self._time_control -= 0.5
        pythoncom.CoUninitialize()

    def transport_usb_insert_entites_to_file(self, period_scan=False):
        status = 1
        try_number = 1
        while status == 1 and try_number <= 5:
            try:
                devices_list = self.list_all_connected_devices()
                self._device_analyzer.control_usb_devices(devices_list, period_scan=period_scan)
                status = 0
            except wmi.x_wmi as e:
                self._logger.info("USB suddenly took out when wmi listing devices, info: %s", e.info)
                status = 1
            except Exception as e:
                self._logger.info("Something when wrong in 'usb insert entities to file' method, info: %s", e.args)
                status = 1
            try_number += 1

        if status == 0:
            # write device information
            self._write_device_information()
            # write memory information - provision
            self._remote_memory_tracker.write_cached_remote_memory_to_file()
            # write files in memory information - provision
            self._remote_memory_tracker.write_cached_files_to_file()

    def _write_device_information(self):
        if len(self._device_analyzer.storage_container_to_write) > 0:
            usb_devices_insert_to_writes = copy(self._device_analyzer.storage_container_to_write)
            self._device_analyzer.storage_container_to_write.clear()
            write_status = self._device_writer.write_usb_devices_insert_info(usb_devices_insert_to_writes,
                                                                             Parameters.USB_DEVICES_INFO_FILE_NAME)
            if write_status == 1:
                self._device_analyzer.storage_container_to_write.extend(usb_devices_insert_to_writes)

    # this method stop tracking by stop 'while' loop but don't stop windows function (event)
    # so application stopped after some USB event, because windows function stopped then.
    def stop_usb_by_flag(self, stop_flag: bool):
        self._process_watcher_stop = stop_flag

    # this method immediately stop tracking usb and stop windows function,
    # but the minus is that stop all application. This method kill application process.
    def stop_usb_by_terminate_pid_process(self, pid: int):
        windows_method_container = wmi.WMI()
        for process in windows_method_container.Win32_Process(ProcessId=pid):
            try:
                process.Terminate()
                self._logger.info("Process terminated successfully: %d", process.ProcessId)
            except Exception as e:
                self._logger.error("There is no process running with id: %d", pid)
                self._logger.error("Exception information: ", e.args)

    def block_usb_operation_by_exe(self):
        pythoncom.CoInitialize()
        if self._previous_block_usb_status != self._current_block_usb_status or self._app_start is True:
            self._previous_block_usb_status = self._current_block_usb_status
            self._app_start = False
            try:
                if self._current_block_usb_status:
                    self._logger.debug('Block USB ')
                    self.disable_usb_ports_by_exe()
                else:
                    self._logger.debug('Enable USB ')
                    self.enable_usb_ports_by_exe()
            except Exception as e:
                self._logger.error('Block USB operations throw exception, info: %s', e.args)
        pythoncom.CoUninitialize()

    @staticmethod
    def disable_usb_ports_by_exe():
        # from logIT.usb_module import USB_MOD_PATH
        # current_path = USB_MOD_PATH
        # psexec_path = current_path + '\\blocking\\PsExec.exe'
        psexec_path = os.getcwd() + '\\PsExec.exe'
        # -windowstyle hidden
        param = ' -d -i -s '  # or  -u {} -p {} -i -s
        # script_reg = current_path + '\\blocking\\disable_USB_ports.bat'
        script_reg = 'REG ADD HKLM\\SYSTEM\\CurrentControlSet\\Services\\USBSTOR /v Start /t REG_DWORD /d 4 /f'
        try:
            subprocess.Popen(psexec_path + param + script_reg, shell=True)
        except:
            _log = Logger.create_to_file_logger(logging_name='Block USB')
            _log.warning("Problem with blocking")

    @staticmethod
    def enable_usb_ports_by_exe():
        # from logIT.usb_module import USB_MOD_PATH
        # current_path = USB_MOD_PATH
        # psexec_path = current_path + '\\blocking\\PsExec.exe'
        psexec_path = os.getcwd() + '\\PsExec.exe'
        param = ' -d -i -s '  # or  -u {} -p {} -i -s
        script_reg = 'REG ADD HKLM\\SYSTEM\\CurrentControlSet\\Services\\USBSTOR /v Start /t REG_DWORD /d 3 /f'
        # script_reg = current_path + '\\blocking\\enable_USB_ports.bat'
        try:
            subprocess.Popen(psexec_path + param + script_reg, shell=True)
        except:
            _log = Logger.create_to_file_logger(logging_name='Enable USB')
            _log.warning("Problem with enable")

    # TODO - resolve problem with expect for windows
    def enable_usb_port_by_pass(self, admin: str, password: str):
        # import winpexpect
        # runas = 'runas /user:'
        # admin_login = ''
        # admin_password = ''
        # print(runas + admin_login + ' ' + script_reg)
        # #print(psexec_path + ' ' + admin_login + ' ' + script_reg)
        # cmd = 'cmd.exe'
        # session = winpexpect.winspawn(runas + admin_login + ' ' + script_reg)
        # #session.sendline(runas + admin_login + ' ' + script_reg)
        # session.expect('Password:')
        # session.sendline(admin_password)
        # #print(cos)
        # #subprocess.Popen(psexec_path + param + script_reg, shell=True)
        pass

    # TODO - resolve problem with expect for windows
    def disable_usb_port_by_pass(self, admin: str, password: str):
        pass

    def list_all_connected_devices(self) -> list:
        disk_devices_list = []

        wql = "Select * From Win32_USBControllerDevice"
        windows_method_container = wmi.WMI()
        for item in windows_method_container.query(wql):
            disk_devices_list.append(item)

        return disk_devices_list
