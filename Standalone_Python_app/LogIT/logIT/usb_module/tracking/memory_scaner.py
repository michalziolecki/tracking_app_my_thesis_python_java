from logIT.develop_log.dev_log import Logger
from logIT.usb_module.classes.scan_class import ScanFileEntity
from logIT.usb_module.file_operations.usb_file_operations import USBDevicesWriter
from logIT.application_parameters.Parameters import Parameters
import os
import filetype
from binaryornot import check
from copy import copy


class MemoryScanner:

    def __init__(self, disk_to_scan: str, finger_print: dict, file_container_to_write: list,
                 file_container_to_db: list, connection_with_db_status: bool):
        self._file_container_to_write = file_container_to_write
        self._file_container_to_db = file_container_to_db
        self._disk_to_scan = disk_to_scan
        self._finger_print = finger_print
        self._connection_with_db_status = connection_with_db_status
        self._writer = USBDevicesWriter()
        self._logger = Logger.create_to_file_logger(logging_name='MemoryScanner')

    def scan_memory(self, scanner_thread_counter: set) -> None:
        scanner_thread_counter.add(self)
        files_list = []
        try:
            root_to_scan = self._disk_to_scan + "\\"
            for root, dirs, files in os.walk(root_to_scan):
                try:
                    if os.path.isdir(root):
                        for filename in files:
                            try:
                                file_path = root + '\\' + filename
                                file_type, file_extension = self.check_file_type(file_path)
                                parameters = {
                                    'scanned_disk': self._disk_to_scan,
                                    'file_name': filename,
                                    'path_name': root,
                                    'file_type': file_type,
                                    'file_extension': file_extension,
                                    'is_binary': self.is_binary_file(file_path),
                                    'file_size': self.get_size_of_file(file_path)
                                }
                                file_obj = ScanFileEntity(self._finger_print, parameters)
                                files_list.append(file_obj)
                                if len(files_list) > 500 and self._connection_with_db_status:
                                    self._file_container_to_db.extend(files_list)
                                    files_list.clear()
                                elif len(files_list) > 500 and not self._connection_with_db_status:
                                    self._file_container_to_write.extend(files_list)
                                    files_list.clear()
                            except OSError as e:
                                self._logger.warning("Not found file %s", str(e.args))
                except:
                    self._logger.warning("Something went wrong with one of directories: %s", self._disk_to_scan)

        except Exception as e:
            self._logger.warning("Disk disappear suddenly, exception info: " + str(e.args))
            scanner_thread_counter.remove(self)

        # this container is a reference to RemoteMemoryTracker container
        self._file_container_to_write.extend(files_list)
        self._write_cached_files_to_file()
        try:
            scanner_thread_counter.remove(self)
        except:
            self._logger.debug("Exception when app release the thread resource in scan memory")
            scanner_thread_counter.clear()
        self._logger.debug("Scan end: " + self._disk_to_scan)
        # multithreading function stop thread when method call return
        return

    def _write_cached_files_to_file(self):
        if len(self._file_container_to_write) > 0:
            files_info_to_write = copy(self._file_container_to_write)
            self._file_container_to_write.clear()
            write_status = self._writer.write_usb_devices_insert_info(files_info_to_write,
                                                                      Parameters.REMOTE_MEMORY_FILES_FILE_NAME)
            if write_status == 1:
                self._file_container_to_write.extend(files_info_to_write)

    def check_file_type(self, file_path: str) -> tuple:
        try:
            kind = filetype.guess(file_path)
        except FileNotFoundError as fnf:
            self._logger.warning("Not found files: " + fnf.filename)
            return "Not Found", "Not Found"
        except Exception as e:
            self._logger.warning("Some exceptions in guessing type file: " + str(e.args))
            return "None", "None"

        try:
            file_type = copy(str(kind.mime))
        except:
            file_type = "Not Found"

        try:
            file_extension = copy(str(kind.extension))
        except:
            file_extension = "Not Found"

        return file_type, file_extension

    def is_binary_file(self, file_path: str) -> bool:
        try:
            status = check.is_binary(file_path)
        except FileNotFoundError as fnf:
            self._logger.warning("Not found files: " + fnf.filename)
            return False
        except Exception as e:
            self._logger.warning("Some exceptions in guessing type file: " + str(e.args))
            return False

        return status

    def get_size_of_file(self, file_path: str) -> int:

        try:
            file_size = os.stat(file_path).st_size
        except FileNotFoundError as fnf:
            self._logger.warning("Not found files: " + fnf.filename)
            return 0
        except Exception as e:
            self._logger.warning("Some exceptions in guessing type file: " + str(e.args))
            return 0

        return file_size
