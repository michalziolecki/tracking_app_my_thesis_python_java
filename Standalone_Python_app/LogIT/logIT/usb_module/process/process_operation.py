import psutil
import wmi


def get_process_pid(name: str) -> int:
    try:
        for process in psutil.process_iter():

            if process.name() == name:
                return process.pid
    except psutil.NoSuchProcess:
        pass
    return -1


def kill_process_by_pid(pid):
    if pid != -1:
        windows_method_container = wmi.WMI()
        for process in windows_method_container.Win32_Process(ProcessId=pid):
            try:
                process.Terminate()
            except wmi.x_wmi:
                pass
