from wmi import _wmi_object
from logIT.usb_module.enums.enum_insert_stat import InsertStatus
from logIT.usb_module.enums.enum_recognized_dev import Category


class AbstractDevice:
    def __init__(self, usb_object: _wmi_object, _dev_recognize_as: str, insert_status: str):
        self._dev_recognize_as = _dev_recognize_as
        self._insert_status = insert_status
        self._dev_name = usb_object.Dependent.Name
        self._description = usb_object.Dependent.Description
        self._manufacturer = usb_object.Dependent.Manufacturer
        self._device_id = usb_object.Dependent.DeviceID
        self._guid = usb_object.Dependent.ClassGuid
        try:
            self._dev_type = usb_object.Dependent.PNPClass
        except:
            self._dev_type = usb_object.Dependent.Caption

    @property
    def dev_recognize_as(self) -> str:
        return self._dev_recognize_as

    @property
    def insert_status(self) -> str:
        return self._insert_status

    @property
    def dev_name(self) -> str:
        return self._dev_name

    @property
    def description(self) -> str:
        return self._description

    @property
    def manufacturer(self) -> str:
        return self._manufacturer

    @property
    def device_id(self) -> str:
        return self._device_id

    @property
    def guid(self) -> str:
        return self._guid

    @property
    def dev_type(self) -> str:
        return self._dev_type

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash((self._dev_name, self._device_id))

    def get_full_dict_of_entity(self) -> dict:
        return self.__dict__


class Printer(AbstractDevice):

    def __init__(self, usb_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(usb_object, Category.PRINTER.value, insert_status)


class Mouse(AbstractDevice):

    def __init__(self, usb_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(usb_object, Category.MOUSE.value, insert_status)


class Pendrive(AbstractDevice):

    def __init__(self, usb_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(usb_object, Category.PENDRIVE.value, insert_status)


class MassUSBDisk(AbstractDevice):

    def __init__(self, usb_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(usb_object, Category.MASS_DISK.value, insert_status)


class Keyboard(AbstractDevice):

    def __init__(self, usb_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(usb_object, Category.KEYBOARD.value, insert_status)


class Phone(AbstractDevice):

    def __init__(self, usb_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(usb_object, Category.PHONE.value, insert_status)


class Camera(AbstractDevice):

    def __init__(self, usb_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(usb_object, Category.CAMERA.value, insert_status)


class Card(AbstractDevice):
    def __init__(self, usb_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(usb_object, Category.CARD.value, insert_status)


class Headphone(AbstractDevice):
    def __init__(self, usb_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(usb_object, Category.HEADPHONE.value, insert_status)


class UncategorizedDev(AbstractDevice):
    def __init__(self, usb_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(usb_object, Category.UNCATEGORIZED.value, insert_status)
