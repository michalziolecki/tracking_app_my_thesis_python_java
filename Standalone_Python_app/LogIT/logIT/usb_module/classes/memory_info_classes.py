from wmi import _wmi_object
from logIT.usb_module.enums.enum_insert_stat import InsertStatus


class AbstractRemoteMemoryInfo:
    def __init__(self, memory_object: _wmi_object, _dev_recognize_as: str, insert_status: str):
        self._dev_recognize_as = _dev_recognize_as
        self._insert_status = insert_status
        self._disk_name = memory_object.Name
        self._description = memory_object.Description
        self._file_system = memory_object.FileSystem
        self._free_space = memory_object.FreeSpace
        self._max_size = memory_object.Size
        self._volume_name = memory_object.VolumeName
        self._volume_serial_number = memory_object.VolumeSerialNumber

    @property
    def dev_recognize_as(self) -> str:
        return self._dev_recognize_as

    @property
    def insert_status(self) -> str:
        return self._insert_status

    @property
    def disk_name(self) -> str:
        return self._disk_name

    @property
    def description(self) -> str:
        return self._description

    @property
    def file_system(self) -> str:
        return self._file_system

    @property
    def free_space(self) -> str:
        return self._free_space

    @property
    def max_size(self) -> str:
        return self._max_size

    @property
    def volume_name(self) -> str:
        return self._volume_name

    @property
    def volume_serial_number(self) -> str:
        return self._volume_serial_number

    def __eq__(self, other):
        return isinstance(other, AbstractRemoteMemoryInfo) and self.disk_name == other.disk_name
        # return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash(
            (self._dev_recognize_as, self._disk_name, self._insert_status, self._file_system))

    def get_full_dict_of_entity(self) -> dict:
        return self.__dict__


class RemoteDiskMemoryInf(AbstractRemoteMemoryInfo):
    def __init__(self, memory_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(memory_object, 'USB Memory Device', insert_status)


class RemoteCDMemoryInf(AbstractRemoteMemoryInfo):
    def __init__(self, memory_object: _wmi_object, insert_status=InsertStatus.set_in.value):
        super().__init__(memory_object, 'Disk Driver', insert_status)
