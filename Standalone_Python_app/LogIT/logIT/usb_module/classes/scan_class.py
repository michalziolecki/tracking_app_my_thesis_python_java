class ScanFileEntity:

    def __init__(self, finger_print: dict,  parameters: dict):
        self._nick_name = finger_print['nick_name']
        self._host_name = finger_print['host_name']
        self._date_time_info = finger_print['date_time_info']
        self._scanned_disk = parameters['scanned_disk']
        self._file_name = parameters['file_name']
        self._path_name = parameters['path_name']
        self._file_type = parameters['file_type']
        self._is_binary = parameters['is_binary']
        self._file_extension = parameters['file_extension']
        self._file_size = parameters['file_size']

    @property
    def nick_name(self) -> str:
        return self._nick_name

    @property
    def host_name(self) -> str:
        return self._host_name

    @property
    def date_time_info(self) -> str:
        return self._date_time_info

    @property
    def scanned_disk(self) -> str:
        return self._scanned_disk

    @property
    def file_name(self) -> str:
        return self._file_name

    @property
    def path_name(self) -> str:
        return self._path_name

    @property
    def file_type(self) -> str:
        return self._file_type

    @property
    def is_binary(self) -> bool:
        return self._is_binary

    @property
    def file_extension(self) -> str:
        return self._file_extension

    @property
    def file_size(self) -> int:
        return self._file_size

    def get_full_dict_of_entity(self) -> dict:
        return self.__dict__


