from cryptography.fernet import Fernet


class Encryption:
    def __init__(self, key):
        self.key = key

    def _generate_key(self) -> str:
        key = Fernet.generate_key()
        return key

    def encrtpted(self, text) -> str:
        f = Fernet(self.key)
        encrypted = f.encrypt(text.encode())
        return encrypted.decode("utf-8")

    def decrypted(self, text_encrypted) -> str:
        f = Fernet(self.key)
        decrypted = f.decrypt(text_encrypted.encode())
        return decrypted.decode("utf-8")
