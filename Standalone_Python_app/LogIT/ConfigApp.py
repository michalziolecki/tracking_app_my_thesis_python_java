from logIT.application_parameters.Config import Config

Config().run(False)
from logIT.application_parameters.Parameters import *
from tkinter import *
import json
from logIT.application_parameters.application_param import *
import ctypes
import sys


def _search_appdata(text) -> str:
    find = "\AppData\Roaming"
    x = text.find(find)
    if x >= 0:
        text = "%%appdata%%" + text[text.find(find) + len(find):len(text)]
    return text


def _percent_to_double_percent(text) -> str:
    text_new = ''
    for char in text:
        if char == '%':
            text_new += char
            text_new += '%'
        else:
            text_new += char
    return text_new


window = Tk()
window.title("LogIT Config")
window.geometry('1200x550')
window.minsize(1200, 550)
window.maxsize(1200, 550)
window.resizable(0, 0)
# window.columnconfigure(0, weight=1)

"""     DataBase    """
label_DataBase_Section = Label(window, text="DataBase", font=("bold", 16))
label_DataBase_Section.place(x=245, y=0)
#  Serwer SQL address
label_address = Label(window, text="Serwer SQL:", font=("bold", 10))
label_address.place(x=25, y=30)
txt_address = Entry(window, width=72, font=('bold', 8))
txt_address.place(x=125, y=30)
txt_address.insert(0, Parameters.DB_ADDRESS)
#  DataBaseName
label_db_name = Label(window, text="DataBase name:", font=("bold", 10))
label_db_name.place(x=25, y=55)
txt_db_name = Entry(window, width=68, font=('bold', 8))
txt_db_name.place(x=150, y=55)
txt_db_name.insert(0, Parameters.DB_NAME)
#  Port SQL
label_port = Label(window, text="Port SQL: ", font=("bold", 10))
label_port.place(x=25, y=80)
numeric_port = Spinbox(window, width=75, from_=0, to=99999, font=('bold', 8))
numeric_port.place(x=100, y=80)
numeric_port.delete(0)
numeric_port.insert(0, Parameters.DB_PORT)
#  login SQL
label_login = Label(window, text="Login SQL:", font=("bold", 10))
label_login.place(x=25, y=105)
txt_login_sql = Entry(window, width=75, font=('bold', 8))
txt_login_sql.place(x=110, y=105)
txt_login_sql.insert(0, Parameters.DB_LOGIN)
#  Hasło SQL
label_password = Label(window, text="Hasło SQL:", font=("bold", 10))
label_password.place(x=25, y=130)
txt_password_sql = Entry(window, width=75, show='*', font=('bold', 8))
txt_password_sql.place(x=110, y=130)
txt_password_sql.insert(0, Parameters.DB_PASSWORD)

"""     Files    """
label_DataBase_Section = Label(window, text="Pliki", font=("bold", 16))
label_DataBase_Section.place(x=265, y=150)
#  log_path
label_log_path = Label(window, text="Ścieżka Loga:", font=("bold", 10))
label_log_path.place(x=25, y=180)
txt_log_path = Entry(window, width=72, font=('bold', 8))
txt_log_path.place(x=130, y=180)
txt_log_path.insert(0, _search_appdata(Parameters.LOG_PATH))
#  log_file_name
label_file_name = Label(window, text="Nazwa Folderu:", font=("bold", 10))
label_file_name.place(x=25, y=205)
txt_file_name = Entry(window, width=70, font=('bold', 8))
txt_file_name.place(x=140, y=205)
txt_file_name.insert(0, Parameters.LOG_FILE_NAME)
#  keylog_file_path
label_keylog_file_path = Label(window, text="Ściezka plików json:", font=("bold", 10))
label_keylog_file_path.place(x=25, y=230)
txt_keylog_file_path = Entry(window, width=65, font=('bold', 8))
txt_keylog_file_path.place(x=170, y=230)
txt_keylog_file_path.insert(0, _search_appdata(Parameters.KEYLOG_FILE_PATH))
#  keylog_file_name
label_keylog_file_name = Label(window, text="Nazwa keyloggera:", font=("bold", 10))
label_keylog_file_name.place(x=25, y=255)
txt_keylog_file_name = Entry(window, width=66, font=('bold', 8))
txt_keylog_file_name.place(x=163, y=255)
txt_keylog_file_name.insert(0, Parameters.KEYLOG_FILE_NAME)
#  USB_device_name
label_usb_device_name = Label(window, text="Nazwa usb:", font=("bold", 10))
label_usb_device_name.place(x=25, y=280)
txt_usb_device_name = Entry(window, width=75, font=('bold', 8))
txt_usb_device_name.place(x=110, y=280)
txt_usb_device_name.insert(0, Parameters.USB_DEVICES_INFO_FILE_NAME)
#  Remonte Memory
label_remote_memory = Label(window, text="Nazwa urządzenia:", font=("bold", 10))
label_remote_memory.place(x=25, y=305)
txt_remote_memory = Entry(window, width=66, font=('bold', 8))
txt_remote_memory.place(x=163, y=305)
txt_remote_memory.insert(0, Parameters.REMOTE_MEMORY_INFO_FILE_NAME)
#  Remote Memory File
label_remote_file = Label(window, text="Nazwa plików:", font=("bold", 10))
label_remote_file.place(x=25, y=330)
txt_remote_file = Entry(window, width=71, font=('bold', 8))
txt_remote_file.place(x=132, y=330)
txt_remote_file.insert(0, Parameters.REMOTE_MEMORY_FILES_FILE_NAME)

"""    USB    """
label_DataBase_Section = Label(window, text="USB", font=("bold", 16))
label_DataBase_Section.place(x=865, y=0)
#   untracked partitions
label_untracked_partitions = Label(window, text='Partycje nieśledzone, np.["C:","D"]:', font=('bold', 10))
label_untracked_partitions.place(x=625, y=30)
txt_untracked_partitions = Entry(window, width=51, font=('bold', 8))
txt_untracked_partitions.place(x=850, y=30)
txt_untracked_partitions.insert(0, json.dumps(Parameters.UNTRACKED_PARTITIONS_IN_OFFLINE))
#  USB devices time
label_usb_devices_time = Label(window, text="Co ile zbierać informacje o urządzeniach:", font=('bold', 10))
label_usb_devices_time.place(x=625, y=55)
numeric_usb_devices_time = Spinbox(window, width=44, from_=0, to=99999, font=('bold', 8))
numeric_usb_devices_time.place(x=882, y=55)
numeric_usb_devices_time.delete(0)
numeric_usb_devices_time.insert(0, Parameters.USB_DEVICES_PERIODIC_TIME)
#  disk drivers scan time
label_disk_drivers_scan_time = Label(window, text="Co ile skanować sterowniki:", font=('bold', 10))
label_disk_drivers_scan_time.place(x=625, y=80)
numeric_disk_drivers_scan_time = Spinbox(window, width=48, from_=0, to=99999, font=('bold', 10))
numeric_disk_drivers_scan_time.place(x=810, y=80)
numeric_disk_drivers_scan_time.delete(0)
numeric_disk_drivers_scan_time.insert(0, Parameters.DISK_DRIVERS_SCAN_PERIODIC_TIME)
#   db_device_connectin_time
label_db_device_connection_time = Label(window, text="Co ile sprawdzać połączenie z urządzeniami:", font=('bold', 10))
label_db_device_connection_time.place(x=625, y=105)
numeric_db_device_connection_time = Spinbox(window, width=41, from_=0, to=99999, font=('bold', 8))
numeric_db_device_connection_time.place(x=900, y=105)
numeric_db_device_connection_time.delete(0)
numeric_db_device_connection_time.insert(0, Parameters.DB_DEVICE_PERIODIC_CONNECTION_TIME)

"""    Keylogger    """
label_DataBase_Section = Label(window, text="Keylogger", font=("bold", 16))
label_DataBase_Section.place(x=840, y=125)
#  db keylog peridodic connection time
label_db_keylog_connection_time = Label(window, text="Co ile sprawdzać połączenie SQL dla Keyloggera:",
                                        font=('bold', 10))
label_db_keylog_connection_time.place(x=625, y=155)
numeric_db_keylog_connection_time = Spinbox(window, width=36, from_=0, to=99999, font=('bold', 8))
numeric_db_keylog_connection_time.place(x=930, y=155)
numeric_db_keylog_connection_time.delete(0)
numeric_db_keylog_connection_time.insert(0, Parameters.DB_KEYLOG_PERIODIC_CONNECTION_TIME)
#  file_keylog_writing_time
label_file_writing_time = Label(window, text="Co ile zapisywać do pliku:", font=('bold', 10))
label_file_writing_time.place(x=625, y=180)
numeric_file_writing_time = Spinbox(window, width=58, from_=0, to=99999, font=('bold', 8))
numeric_file_writing_time.place(x=798, y=180)
numeric_file_writing_time.delete(0)
numeric_file_writing_time.insert(0, Parameters.FILE_KEYLOG_PERIODIC_WRITING_TIME)
#  db_keylog_datetime_format
label_keylog_format_datetime = Label(window, text="Format daty, np.%%Y-%%m-%%d:", font=('bold', 10))
label_keylog_format_datetime.place(x=625, y=205)
txt_keylog_format_datetime = Entry(window, width=50, font=('bold', 8))
txt_keylog_format_datetime.place(x=855, y=205)
txt_keylog_format_datetime.insert(0, _percent_to_double_percent(Parameters.DB_KEYLOG_DATETIME_FORMAT))


#  #Button
#  OnClick
def buttonOnClick():
    try:
        #  DataBase
        Parameters.DB_LOGIN = txt_login_sql.get()
        Parameters.DB_PASSWORD = txt_password_sql.get()
        Parameters.DB_ADDRESS = txt_address.get()
        Parameters.DB_PORT = numeric_port.get()
        Parameters.DB_NAME = txt_db_name.get()

        #  Files
        Parameters.LOG_PATH = txt_log_path.get()
        Parameters.LOG_FILE_NAME = txt_file_name.get()
        Parameters.KEYLOG_FILE_PATH = txt_keylog_file_path.get()
        Parameters.KEYLOG_FILE_NAME = txt_keylog_file_name.get()
        Parameters.USB_DEVICES_INFO_FILE_NAME = txt_usb_device_name.get()
        Parameters.REMOTE_MEMORY_INFO_FILE_NAME = txt_remote_memory.get()
        Parameters.REMOTE_MEMORY_FILES_FILE_NAME = txt_remote_file.get()

        #  USB
        Parameters.UNTRACKED_PARTITIONS_IN_OFFLINE = txt_untracked_partitions.get()
        Parameters.USB_DEVICES_PERIODIC_TIME = numeric_usb_devices_time.get()
        Parameters.DISK_DRIVERS_SCAN_PERIODIC_TIME = numeric_disk_drivers_scan_time.get()
        Parameters.DB_DEVICE_PERIODIC_CONNECTION_TIME = numeric_db_device_connection_time.get()

        #  Keylogger
        Parameters.DB_KEYLOG_PERIODIC_CONNECTION_TIME = numeric_db_keylog_connection_time.get()
        Parameters.FILE_KEYLOG_PERIODIC_WRITING_TIME = numeric_file_writing_time.get()
        Parameters.DB_KEYLOG_DATETIME_FORMAT = txt_keylog_format_datetime.get()

        config = Config()
        os.remove(CONFIG_PATH + CONFIG_FILE)
        config.create_file(True)
        ctypes.windll.user32.MessageBoxW(0, "Zapisano zmiany!", "Info", 0)
        sys.exit()
    except Exception as ex:
        ctypes.windll.user32.MessageBoxW(0, ex, "Error", 0)


#  Button
btn = Button(window, text="Zapisz", font=('bold', 12), width=20, command=buttonOnClick)
btn.place(x=970, y=480)

window.mainloop()
