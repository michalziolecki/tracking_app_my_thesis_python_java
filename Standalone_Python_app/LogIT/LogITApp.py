from logIT.application_parameters.Config import Config
Config().run(False)
from logIT.controllers.logIT_controller import LogITController
from logIT.develop_log.dev_log import Logger


def main():
    _logger = Logger.create_to_file_logger(logging_name='main_function')
    try:
        log_it_controller = LogITController()
        log_it_controller.start_application()
    except BaseException as e:
        err = "There was an exception in main method, application stopped"
        _logger.error(err + '\n' + str(e.args))


if __name__ == '__main__':
    main()
