# README #

Uniwersytet Mikołaja Kopernika w Toruniu

Wydział Matematyki i Informatyki

Michał Ziółecki
Nr albumu: 290489
Kierunek: Informatyka

Praca Licencjacka: 
"Aplikacja śledząca urządzenia peryferyjne użytkownika oraz keylogger rejestrujący klawiaturę"

Opiekun pracy dyplomowej: dr Marta Jadwiga Burzańska

Toruń 2019

# Projekt składa się z trzech podstawowych elementów:
#	- Bazy Danych,
#	- Aplikacji serwerowej Java,
#	- Aplikacji desktopowej (standalone) Python.

# Układ projektu:
#   - Katalog "Data_Base_Postgres":
#	   -> Zawiera skrypty bazowanowe. W celu stworzenia bazy danych należy je uruchomić w kolejności z godnej z numeracją plików.
#	- Katalog "Presentation_Java":
#      -> Zawiera w sobie główny katalog projektu aplikacji serwerowej w Java. Katalog ten nazywa się "LogITPresentation"
#	   -> Katalog "LogITPresentation":
#          * Z tego miejsca istnieje możliwoćc uruchomienia aplikacji serwerowej komendą konsolową z użyciem Apache Maven - "mvn spring-boot:run"
#	       * Uruchomienie aplikacji wymaga przygotowania środowiska zgodnie z opisem w pracy dyplomowej.
#	- Katalog "Standalone_Python_app":
#      -> Zawiera katalog z nazwą "LogIT", który jest głównym katalogie projektu LogITApp. Zawiera on również pliki binarne,
#		  po kompilacji narzędzeniem PyInstaller.
#      -> Katalog "LogIT":
		   * jest to głowny katalog projektu
		   * Katalog "dist" zawiera instalator o nazwie LogITSetup.exe oraz pliki skopilowanej aplikacji w podkatalogu "LogTIApp".
		   * Katalog LogITApp zawiera całość aplikacji oraz dwa pliki uruchomieniowe LogTIApp.exe oraz ConfigApp.exe.    
